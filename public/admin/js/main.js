$(function() {
      var baseurl   = $("#base_url").val();

      // Ajax Request
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      $('select.form-control').select2();

      $("#neverEnd").on('click', function(e) {
           var checked = $(this).prop('checked');

           if(checked) {
                $('#validUpto').attr('disabled', 'disabled').val('');
           } else {
                $('#validUpto').removeAttr('disabled');
           }
      });

      $('.check_enable input[type=checkbox]').on('click', function() {
           var checked = $(this).prop('checked'),
               input   = $(this).closest('.check_enable').find('input[type=text]');

          if(checked) {
               input.removeAttr('readonly');
          } else {
               input.attr('readonly', 'readonly');
          }
      });

      // Locations
      $('.country').on('change', function() {
          var target = $(this).data('target');
          $.ajax({
              url: baseurl+'/ajax/get_states/',
              type: 'POST',
              data: {
                  'id': $(this).val()
              },
              success: function(res) {
                  $(target).html( $('<option>').val('').text('Select State') );

                  $.each(res.data, function(i, row) {
                      $(target).append( $('<option>').val(row.state_id).text(row.state_name+' ('+row.state_short_name+')') );
                  });

                  $(target).trigger('change');
              }
          });
      });
      $('.state').on('change', function() {
          var target = $(this).data('target');
          $.ajax({
              url: baseurl+'/ajax/get_cities/',
              type: 'POST',
              data: {
                  'id': $(this).val()
              },
              success: function(res) {
                  $(target).html( $('<option>').val('').text('Select City') );

                  $.each(res.data, function(i, row) {
                      $(target).append( $('<option>').val(row.city_id).text(row.city_name+' ('+row.city_short_name+')') );
                  });

                  $(target).trigger('change');
              }
          });
      });
      // End Location

      $('.service').on('change', function() {
          var target = $(this).data('target');
          $.ajax({
              url: baseurl+'/ajax/get_subservices/',
              type: 'POST',
              data: {
                  'id': $(this).val()
              },
              success: function(res) {
                  $(target).html( $('<option>').val('').text('Select Subservices') );

                  $.each(res.data, function(i, row) {
                      $(target).append( $('<option>').val(row.subservice_id).text(row.subservice_name) );
                  });

                  $(target).trigger('change');
              }
          });
      });

      // Datepicker
      $( ".datepicker" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd',
      });

      $( ".datepicker2" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd',
          minDate: 0
      });

      $( ".datepicker3" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd',
          maxDate: 0
      });

      $( ".datepicker_dob" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd',
          maxDate: -3650
      });
      // End Datepicker

      $('.left_sidebar>ul li.dropdown>a').on('click', function(e) {
          e.preventDefault();

          var li = $(this).parent();
          $(this).next().slideToggle(500);
          li.toggleClass('active');
      });

      $('[data-toggle="tooltip"]').tooltip();

      $('a[href="#refresh"]').on('click', function(e) {
          e.preventDefault();

          location.reload();
      });

      $('a[href="#save-data"]').on('click', function(e) {
          e.preventDefault();

          $(this).closest('form').trigger('submit');
      });

      $(".checkall").on("click", function() {
          var form = $(this).closest("form");

          form.find(".check:not([disabled])").prop( "checked", $(this).prop("checked") );
      });

      $(".check").on("click", function() {
          var form = $(this).closest("form");
          var checked = (form.find('.check').length == form.find('.check:checked').length);

          form.find(".checkall").prop( "checked", checked );
      });

      $("#service_man_id").on("change", function() {
          if($(this).val() != "") {
              var form = $(this).closest("form");
              if(form.find(".check:checked").length > 0) {
                  swal({
                      title: "Are you sure?",
                      text: "You're going to assign order to service man!",
                      icon: "warning",
                      buttons: true,
                      dangerMode: true,
                  })
                  .then((willDelete) => {
                      if (willDelete) {
                          form.trigger("submit");
                      }
                  });
              } else {
                  swal("Warning", "Select at least one record to assign", "warning");
              }
          }
      });

      $("a[href='#remove']").on("click", function (e) {
          e.preventDefault();

          var form = $(this).closest("form");
          if(form.find(".check:checked").length > 0) {
              swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover record(s)!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
              })
                  .then((willDelete) => {
                      if (willDelete) {
                          form.trigger("submit");
                      }
                  });
          } else {
              swal("Warning", "Select at least one record to delete", "warning");
          }

      });

      $(".upload_image input[type=file]").change(function() {
          var target = $(this).closest(".upload_image").find("img");
          readURL(this, target);
      });

      $("#loginForm").on("submit", function(e) {
          e.preventDefault();

          var form     = $(this);
          var is_valid = form.is_valid();
          var fmsg     = form.find('.form-msg');
          var action   = form.attr('action');

          if(is_valid) {
              fmsg.addClass('alert alert-info').removeClass('alert-danger alert-success').html('Progressing, please wait...');

              $.ajax({
                  url: action,
                  type: 'POST',
                  data: form.serialize(),
                  success: function(res) {
                      if(res.status) {
                          fmsg.removeClass('alert-info').addClass('alert-success').html(res.message);
                          location.href = "";
                      } else {
                          fmsg.removeClass('alert-info').addClass('alert-danger').html(res.message);
                      }
                  }
              });
          }
      });

      $('[href="#nav-open"]').on('click', function(e) {
          e.preventDefault();

          $('.left_sidebar').toggleClass('open');
      });

      tinyMCE.PluginManager.add('stylebuttons', function(editor, url) {
        ['div', 'pre', 'p', 'code', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'].forEach(function(name){
          editor.addButton("style-" + name, {
              tooltip: "Toggle " + name,
                text: name.toUpperCase(),
                onClick: function() { editor.execCommand('mceToggleFormat', false, name); },
                onPostRender: function() {
                    var self = this, setup = function() {
                        editor.formatter.formatChanged(name, function(state) {
                            self.active(state);
                        });
                    };
                    editor.formatter ? setup() : editor.on('init', setup);
              }
          })
        });
      });
      tinymce.init({
          selector: '.editor',
          plugins: "code, link, image, textcolor, emoticons, hr, lists, stylebuttons, charmap",
          fontsizeselect: true,
          browser_spellcheck: true,
          menubar: false,
          toolbar: 'bold italic underline strikethrough | style-h1 style-h2 style-h3 | hr superscript subscript | alignleft aligncenter alignright alignjustify bullist numlist outdent indent code' ,
          // forced_root_block : 'div',
          branding: false,
          protect: [
              /\<\/?(if|endif)\>/g,  // Protect <if> & </endif>
              /\<xsl\:[^>]+\>/g,  // Protect <xsl:...>
              /\<script\:[^>]+\>/g,  // Protect <xsl:...>
              /<\?php.*?\?>/g  // Protect php code
          ],
          images_upload_credentials: true,
          file_browser_callback_types: 'image',
          image_dimensions: true,
          automatic_uploads: true,
          relative_urls : false,
          remove_script_host : false,
          images_reuse_filename: true,
      });

      // Website JS
      $('.add_wish_row').on('click', function() {
          var   html = '<div class="row form-group">';
                html += '<div class="col-sm-10 col-9">';
                html += '<input type="text" name="record[service_wish][]" value="" class="form-control" placeholder="Wishes">';
                html += '</div>';
                html += '<div class="col-sm-2 col-3 text-center">';
                html += '<button type="button" class="btn btn-block btn-danger remove_wish_row"> <i class="icon-minus-circle"></i> </button>';
                html += '</div>';
                html += '</div>';

            $('#wishRow').append( html );
      });

      $(document).on('click', '.remove_wish_row', function() {
            $(this).closest('.row').remove();
      });
      var i = $("#totalAddRow").val();
      $('.add_addr_row').on('click', function() { i++;
          var   html  = '<div class="row">';
                html += '<div class="col-sm-5">';
                html += '<div class="form-group">';
                html += '<label>House / Flat No.</label>';
                html += '<input type="text" name="addr['+i+'][uaddr_address1]" value="" placeholder="House / Flat No." class="form-control">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-5">';
                html += '<div class="form-group">';
                html += '<label>Location (Required)</label>';
                html += '<input type="text" name="addr['+i+'][uaddr_address2]" value="" placeholder="Location" class="form-control" required>';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-2">';
                html += '<div class="form-group">';
                html += '<label class="d-none d-sm-block d-md-block d-lg-block">&nbsp;</label>';
                html += '<button type="button" class="btn btn-danger btn-block remove_addr_row"> <i class="icon-minus-circle"></i> </button>';
                html += '</div>';
                html += '</div>';
                html += '</div>';

            $('#addressRows').append( html );
      });

      $(document).on('click', '.remove_addr_row', function() {
            $(this).closest('.row').remove();
      });
      // End Website JS
});

function readURL(input, target) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $(target).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
