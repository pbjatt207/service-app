<?php
Route::get('service-panel/user/logout', function() {
    session()->forget('user_auth');
    return redirect( url('service-panel') );
});

Route::get('service-panel', 'backend\Dashboard@index');
Route::any('service-panel/category/{slug?}', 'backend\Category@index');
Route::any('service-panel/service/subservices', 'backend\Service@subservices');
Route::any('service-panel/service/add_subservice/{slug?}', 'backend\Service@add_subservice');
Route::any('service-panel/service/{slug?}', 'backend\Service@index');

Route::any('service-panel/milestone/{id?}', 'backend\MilestoneController@index');

Route::any('service-panel/page/edit/{slug?}', 'backend\Page@edit');

// User
Route::any('service-panel/user/add/{role?}/{id?}', 'backend\User@add');
Route::any('service-panel/user/add-partner/{id?}', 'backend\User@add_partner');
Route::any('service-panel/partner', 'backend\User@partner');
Route::any('service-panel/user/{role?}', 'backend\User@index');
Route::any('service-panel/change-password', 'backend\User@change_password');

Route::any('service-panel/partner/location/{uid}', 'backend\User@location');

// Slider
Route::any('service-panel/slider/add/{role?}/{id?}', 'backend\Slider@add');
Route::any('service-panel/slider/{role?}', 'backend\Slider@index');

// Why Choose Us
Route::any('service-panel/choose/add/{role?}/{id?}', 'backend\Choose@add');
Route::any('service-panel/choose/{role?}', 'backend\Choose@index');

// Timeslot
Route::any('service-panel/timeslot/{id?}', 'backend\Timeslot@index');

// Wallet
Route::any('service-panel/wallet/add', 'backend\WalletController@add');
Route::any('service-panel/wallet/{id?}', 'backend\WalletController@index');

// Coupon
Route::any('service-panel/coupon/add/{id?}', 'backend\Coupon@add');
Route::any('service-panel/coupon/', 'backend\Coupon@index');

// Order
Route::any('service-panel/order/customer', 'backend\Order@customer');
Route::any('service-panel/order/add/{uid}/{id?}', 'backend\Order@add');
Route::any('service-panel/order/', 'backend\Order@index');

Route::any('service-panel/invoice/{oid}', 'backend\InvoiceController@index');

// Location
Route::any('service-panel/location/countries/{slug?}', 'backend\Location@country');
Route::any('service-panel/location/states/{slug?}', 'backend\Location@states');
Route::any('service-panel/location/cities/{slug?}', 'backend\Location@cities');

// Settings
Route::any('service-panel/settings', 'backend\SettingController@index');

Route::any('service-panel/ajax/user_login', 'backend\Ajax@user_login');
Route::any('service-panel/ajax/{action}', 'backend\Ajax@index');
