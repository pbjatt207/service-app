<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('startup', 'api\StartController@index');

// user
Route::post('user/sendotp', 'api\User@sendotp');
Route::post('user/login', 'api\User@login');
Route::get('user/refer/{code}', 'api\User@refer');
Route::post('user/edit-profile/{uid}', 'api\User@edit_profile');
Route::post('user/update-photo', 'api\User@update_photo');
Route::post('add-address', 'api\User@add_address');
Route::get('address/{uid}', 'api\User@view_address');
Route::get('remove-address/{id}', 'api\User@remove_address');
Route::post('save-location', 'api\User@save_location');
// End user

// Services
Route::get('service', 'api\ServiceController@index');
Route::get('subservice/{sid}/{uid?}', 'api\ServiceController@subservices');
// End Services

// Page
Route::get('page/{slug?}', 'api\PageController@index');
Route::get('web-page/{slug?}', 'api\PageController@web');
// End Page

// Cart
Route::get('cart/{uid}', 'api\CartController@index');
Route::get('add-to-cart/{uid}/{sid}', 'api\CartController@add');
// End Cart

// Why Choose Us
Route::get('why-choose-us', 'api\ChooseController@index');
// End Why Choose Us

// Schedule
Route::get('schedule', 'api\ScheduleController@index');
Route::get('schedule-dates/{sid}', 'api\ScheduleController@date_timeslotes');
// End Schedule

// Promo Codes
Route::post('apply-promo-code', 'api\CouponController@apply');
Route::get('promo-code/{user_id}', 'api\CouponController@index');
// End Promo Codes

// Order
Route::post('place-order', 'api\OrderController@add');
Route::post('reschedule-order/{order_id}', 'api\OrderController@edit');
Route::get('cancel-order/{order_id}', 'api\OrderController@cancel');
Route::get('end-service/{order_id}/{payment_mode}/{txn_id?}', 'api\OrderController@completed');
Route::get('fill-happy-code/{order_id}/{happy_code}', 'api\OrderController@fill_happy_code');
Route::post('my-orders', 'api\OrderController@my_order');
Route::get('order-info/{order_id}', 'api\OrderController@order_single');
Route::post('update-order/{endservice?}', 'api\OrderController@edit_order_cart');
Route::get('happy-codes/{user_id}', 'api\OrderController@happy_codes');
// End Order


/*===============================
 * BUSINESS APP
 *===============================*/
Route::post('business/startup', 'api\StartController@business');
Route::get('business/home/{user_id}', 'api\HomeController@index');

 // Partner User
 Route::post('business/sendotp', 'api\User@business_otp_send');
 Route::post('business/login', 'api\User@business_login');
 Route::get('business/profile/{user_id}', 'api\User@business_profile');

// Wallet
Route::get('business/wallet/{user_id}', 'api\WalletController@index');
Route::get('wallet/{user_id}', 'api\WalletController@index');

// Task
Route::get('business/task/{user_id}', 'api\TaskController@index');
Route::get('business/tasks/{type}/{user_id}', 'api\TaskController@tasks');
Route::get('business/oneday-task/{type}/{user_id}', 'api\TaskController@oneday_task');

// Order
Route::post('deny-order', 'api\OrderController@deny');
Route::post('pending-order/{type}', 'api\OrderController@pending');

Route::get('push-notification', 'api\NotificationController@send');
