-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2019 at 05:16 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ghpsoftware`
--

-- --------------------------------------------------------

--
-- Table structure for table `exc_pro_packing`
--

CREATE TABLE `exc_pro_packing` (
  `ppkg_id` int(11) NOT NULL,
  `ppkg_pid` int(11) NOT NULL,
  `ppkg_pkg_id` int(11) NOT NULL,
  `ppkg_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exs_metals`
--

CREATE TABLE `exs_metals` (
  `metal_id` int(11) NOT NULL,
  `metal_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exs_metals`
--

INSERT INTO `exs_metals` (`metal_id`, `metal_name`) VALUES
(1, 'Iron'),
(2, 'Brass'),
(3, 'Aluminium'),
(4, 'Stainless Steel');

-- --------------------------------------------------------

--
-- Table structure for table `exs_packing`
--

CREATE TABLE `exs_packing` (
  `packing_id` int(11) NOT NULL,
  `packing_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exs_packing`
--

INSERT INTO `exs_packing` (`packing_id`, `packing_name`) VALUES
(1, 'Foam'),
(2, 'Bubble'),
(3, 'Foam Caramel'),
(4, 'Corrugated'),
(5, 'Brown paper');

-- --------------------------------------------------------

--
-- Table structure for table `exs_products`
--

CREATE TABLE `exs_products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `product_type` varchar(255) NOT NULL,
  `product_pdf` varchar(255) NOT NULL,
  `product_l_cm` decimal(10,2) NOT NULL,
  `product_w_cm` decimal(10,2) NOT NULL,
  `product_h_cm` decimal(10,2) NOT NULL,
  `product_l_inch` decimal(10,2) NOT NULL,
  `product_w_inch` decimal(10,2) NOT NULL,
  `product_h_inch` decimal(10,2) NOT NULL,
  `product_box_l_cm` decimal(10,2) NOT NULL,
  `product_box_w_cm` decimal(10,2) NOT NULL,
  `product_box_h_cm` decimal(10,2) NOT NULL,
  `product_box_l_inch` decimal(10,2) NOT NULL,
  `product_box_w_inch` decimal(10,2) NOT NULL,
  `product_box_h_inch` decimal(10,2) NOT NULL,
  `product_ip` int(11) NOT NULL,
  `product_mp` int(11) NOT NULL,
  `product_net_weight_wooden_kg` decimal(10,2) NOT NULL,
  `product_net_weight_wooden_lbs` decimal(10,2) NOT NULL,
  `product_net_weight_iron_kg` decimal(10,2) NOT NULL,
  `product_net_weight_iron_lbs` decimal(10,2) NOT NULL,
  `product_net_weight_other_kg` decimal(20,2) NOT NULL,
  `product_net_weight_other_lbs` decimal(20,2) NOT NULL,
  `product_net_weight_kg` decimal(10,2) NOT NULL,
  `product_net_weight_lbs` decimal(10,2) NOT NULL,
  `product_gross_weight_kg` text NOT NULL,
  `product_gross_weight_lbs` text NOT NULL,
  `product_tot_gross_weight_kg` decimal(20,2) NOT NULL,
  `product_tot_gross_weight_lbs` decimal(20,2) NOT NULL,
  `product_cbm` decimal(10,6) NOT NULL,
  `product_cft` decimal(20,4) NOT NULL,
  `product_load_20cont` decimal(20,2) NOT NULL,
  `product_load_40std_cont` decimal(20,2) NOT NULL,
  `product_load_40hc_cont` decimal(20,2) NOT NULL,
  `product_material_wood` varchar(255) NOT NULL,
  `product_metal` varchar(255) NOT NULL,
  `product_glass` varchar(255) NOT NULL,
  `product_fabric` varchar(255) NOT NULL,
  `product_mrp` decimal(20,2) NOT NULL,
  `product_pdf2` varchar(255) NOT NULL,
  `product_text` text NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_image1` varchar(255) NOT NULL,
  `product_image2` varchar(255) NOT NULL,
  `product_image3` varchar(255) NOT NULL,
  `product_image4` varchar(255) NOT NULL,
  `product_image5` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exs_products`
--

INSERT INTO `exs_products` (`product_id`, `product_name`, `product_code`, `product_type`, `product_pdf`, `product_l_cm`, `product_w_cm`, `product_h_cm`, `product_l_inch`, `product_w_inch`, `product_h_inch`, `product_box_l_cm`, `product_box_w_cm`, `product_box_h_cm`, `product_box_l_inch`, `product_box_w_inch`, `product_box_h_inch`, `product_ip`, `product_mp`, `product_net_weight_wooden_kg`, `product_net_weight_wooden_lbs`, `product_net_weight_iron_kg`, `product_net_weight_iron_lbs`, `product_net_weight_other_kg`, `product_net_weight_other_lbs`, `product_net_weight_kg`, `product_net_weight_lbs`, `product_gross_weight_kg`, `product_gross_weight_lbs`, `product_tot_gross_weight_kg`, `product_tot_gross_weight_lbs`, `product_cbm`, `product_cft`, `product_load_20cont`, `product_load_40std_cont`, `product_load_40hc_cont`, `product_material_wood`, `product_metal`, `product_glass`, `product_fabric`, `product_mrp`, `product_pdf2`, `product_text`, `product_image`, `product_image1`, `product_image2`, `product_image3`, `product_image4`, `product_image5`) VALUES
(1, 'Test Product', 'TP23023', 'K/D', '1.pdf', '20.00', '20.00', '20.00', '7.87', '7.87', '7.87', '20.00', '20.00', '20.00', '7.87', '7.87', '7.87', 20, 3, '20.00', '44.09', '20.00', '44.09', '20.00', '44.09', '60.00', '132.27', 'a:3:{i:0;s:5:\"20.00\";i:1;s:5:\"20.00\";i:2;s:5:\"20.00\";}', 'a:3:{i:0;s:5:\"44.09\";i:1;s:5:\"44.09\";i:2;s:5:\"44.09\";}', '60.00', '132.27', '0.152400', '5.3820', '131.23', '360.89', '439.63', 'Acacia', 'Brass', 'Toughened', 'Olive', '500.00', 'PDF2_1.pdf?v=5d148bcabb837', 'fun,comedy,advance', 'IMG_1.jpg?v=5d148c1c56e63', 'IMG1_1.jpg?v=5d0b53f1e3368', 'IMG2_1.jpg?v=5d148c1c69a34', 'IMG3_1.jpg?v=5d148c1c8205c', 'IMG4_1.jpg?v=5d148c1c987c1', 'IMG5_1.jpg?v=5d148c1cb1a0f'),
(6, 'Sofa', 'GHP-00342', 'Fixed', '', '15.00', '15.00', '15.00', '5.91', '5.91', '5.91', '15.00', '15.00', '15.00', '5.91', '5.91', '5.91', 2, 1, '15.00', '33.07', '15.00', '33.07', '15.00', '33.07', '45.00', '99.21', '', '', '0.00', '0.00', '0.000000', '0.0000', '0.00', '0.00', '0.00', '', '', '', '', '0.00', '', '', '', '', '', '', '', ''),
(7, '', '', 'Fixed', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '', '0.00', '0.00', '0.000000', '0.0000', '0.00', '0.00', '0.00', '', '', 'None', '', '0.00', '', '', '', '', '', '', '', ''),
(8, '', '', 'Fixed', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '', '0.00', '0.00', '0.000000', '0.0000', '0.00', '0.00', '0.00', '', '', 'None', '', '0.00', '', '', '', '', '', '', '', ''),
(9, '', '', 'Fixed', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '', '0.00', '0.00', '0.000000', '0.0000', '0.00', '0.00', '0.00', '', '', '', '', '0.00', '', '', '', '', '', '', '', ''),
(10, '', '', 'K/D', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', -1, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '', '0.00', '0.00', '0.000000', '0.0000', '0.00', '0.00', '0.00', '', '', '', '', '0.00', '', '', '', '', '', '', '', ''),
(11, '', '', 'Fixed', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '', '0.00', '0.00', '0.000000', '0.0000', '0.00', '0.00', '0.00', '', '', 'None', '', '0.00', '', '', '', '', '', '', '', ''),
(12, '', '', 'Fixed', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 0, 0, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '', '', '0.00', '0.00', '0.000000', '0.0000', '0.00', '0.00', '0.00', '', '', 'None', '', '0.00', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `exs_pro_hardware`
--

CREATE TABLE `exs_pro_hardware` (
  `phw_id` int(11) NOT NULL,
  `phw_pid` int(11) NOT NULL,
  `phw_hw_name` varchar(255) NOT NULL,
  `phw_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exs_pro_hardware`
--

INSERT INTO `exs_pro_hardware` (`phw_id`, `phw_pid`, `phw_hw_name`, `phw_value`) VALUES
(14, 1, 'Fastner', 'light'),
(15, 1, 'Wrench', 'w hardware'),
(16, 1, 'Screw-Driver', 'sd hardware');

-- --------------------------------------------------------

--
-- Table structure for table `exs_pro_packing`
--

CREATE TABLE `exs_pro_packing` (
  `ppkg_id` int(11) NOT NULL,
  `ppkg_pid` int(11) NOT NULL,
  `ppkg_pkg_id` int(11) NOT NULL,
  `ppkg_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exs_pro_packing`
--

INSERT INTO `exs_pro_packing` (`ppkg_id`, `ppkg_pid`, `ppkg_pkg_id`, `ppkg_value`) VALUES
(2, 1, 1, 'double');

-- --------------------------------------------------------

--
-- Table structure for table `exs_users`
--

CREATE TABLE `exs_users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_mobile` varchar(255) NOT NULL,
  `user_login` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_is_deleted` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exs_users`
--

INSERT INTO `exs_users` (`user_id`, `user_name`, `user_email`, `user_mobile`, `user_login`, `user_password`, `user_is_deleted`) VALUES
(1, '', '', '', 'excel_software', '$2y$12$cC2OF9qnZrHS0HQ.z8xiiOjHzxCcCY1l4mMOb7VWF/Epjs.smz4FK', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `exs_woods`
--

CREATE TABLE `exs_woods` (
  `wood_id` int(11) NOT NULL,
  `wood_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exs_woods`
--

INSERT INTO `exs_woods` (`wood_id`, `wood_name`) VALUES
(1, 'Acacia'),
(2, 'Mango'),
(3, 'Sheesham'),
(4, 'Oak'),
(5, 'Teak');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `exc_pro_packing`
--
ALTER TABLE `exc_pro_packing`
  ADD PRIMARY KEY (`ppkg_id`);

--
-- Indexes for table `exs_metals`
--
ALTER TABLE `exs_metals`
  ADD PRIMARY KEY (`metal_id`);

--
-- Indexes for table `exs_packing`
--
ALTER TABLE `exs_packing`
  ADD PRIMARY KEY (`packing_id`);

--
-- Indexes for table `exs_products`
--
ALTER TABLE `exs_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `exs_pro_hardware`
--
ALTER TABLE `exs_pro_hardware`
  ADD PRIMARY KEY (`phw_id`);

--
-- Indexes for table `exs_pro_packing`
--
ALTER TABLE `exs_pro_packing`
  ADD PRIMARY KEY (`ppkg_id`);

--
-- Indexes for table `exs_users`
--
ALTER TABLE `exs_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `exs_woods`
--
ALTER TABLE `exs_woods`
  ADD PRIMARY KEY (`wood_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `exc_pro_packing`
--
ALTER TABLE `exc_pro_packing`
  MODIFY `ppkg_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exs_metals`
--
ALTER TABLE `exs_metals`
  MODIFY `metal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `exs_packing`
--
ALTER TABLE `exs_packing`
  MODIFY `packing_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `exs_products`
--
ALTER TABLE `exs_products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `exs_pro_hardware`
--
ALTER TABLE `exs_pro_hardware`
  MODIFY `phw_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `exs_pro_packing`
--
ALTER TABLE `exs_pro_packing`
  MODIFY `ppkg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `exs_users`
--
ALTER TABLE `exs_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exs_woods`
--
ALTER TABLE `exs_woods`
  MODIFY `wood_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
