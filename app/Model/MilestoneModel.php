<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MilestoneModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'milestones';
    protected   $primaryKey     = 'ms_id';

    protected $casts = [
        'ms_service_no'     => 'integer',
        'ms_reward_point'   => 'integer',
    ];
}
