<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CartModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'carts';
    protected   $primaryKey     = 'cart_id';

    protected $casts = [
        'cart_uid'   => 'integer',
        'cart_sid'   => 'integer',
    ];
}
