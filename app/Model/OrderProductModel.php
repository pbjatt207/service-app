<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderProductModel extends Model {
    public $timestamps          = false;
    protected   $table          = 'order_products';
    protected   $primaryKey     = 'opro_id';

    protected $casts = [
        'opro_sid'          => 'integer',
        'opro_qty'          => 'integer',
        'opro_oid'          => 'integer',
        'opro_time'         => 'string',
    ];

    public function service() {
        return $this->hasOne('App\Model\ServiceModel', 'service_id', 'opro_sid');
    }
}
