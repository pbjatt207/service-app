<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CouponModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'coupons';
    protected   $primaryKey     = 'coupon_id';

    protected $casts = [
        'coupon_never_end'              => 'integer',
        'coupon_discount'               => 'double',
        'coupon_is_user_limit'          => 'integer',
        'coupon_limit_no'               => 'integer',
        'coupon_is_total_uses'          => 'integer',
        'coupon_total_uses'             => 'integer',
        'coupon_is_total_cart'          => 'integer',
        'coupon_min_cart_total'         => 'double',
        'coupon_is_max_discount_amt'    => 'integer',
        'coupon_max_discount_amt'       => 'double',
        'coupon_sid'                    => 'integer',
        'coupon_ssid'                   => 'integer',
    ];
}
