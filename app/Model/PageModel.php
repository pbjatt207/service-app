<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PageModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'pages';
    protected   $primaryKey     = 'page_id';
}
