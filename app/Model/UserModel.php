<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'users';
    protected   $primaryKey     = 'user_id';

    protected $casts = [
        'user_country'  => 'integer',
        'user_state'    => 'integer',
        'user_city'     => 'integer',
        'user_pincode'  => 'integer',
    ];

    public function partner() {
        return $this->hasOne('App\Model\PartnerModel', 'partner_uid', 'user_id');
    }
    public function country() {
        return $this->hasOne('App\Model\CountryModel', 'country_id', 'user_country');
    }
    public function state() {
        return $this->hasOne('App\Model\StateModel', 'state_id', 'user_state');
    }
    public function city() {
        return $this->hasOne('App\Model\CityModel', 'city_id', 'user_city');
    }
    public function locations() {
        return $this->hasMany('App\Model\Location', 'loc_uid', 'user_id');
    }
}
