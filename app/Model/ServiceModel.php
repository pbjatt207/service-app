<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServiceModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'services';
    protected   $primaryKey     = 'service_id';

    protected $casts = [
        'service_cost'          => 'double',
        'service_order_limit'   => 'integer',
    ];
}
