<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TimeslotModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'timeslots';
    protected   $primaryKey     = 'tslot_id';

    protected $casts = [
        'tslot_min_time'    => 'integer',
    ];
}
