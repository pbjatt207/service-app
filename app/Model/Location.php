<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {
    public      $timestamps     = false;
    protected   $table          = 'locations';
    protected   $primaryKey     = 'loc_id';

    protected $casts = [
        'loc_uid'   => 'integer',
    ];

    public function user() {
       return $this->hasOne('App\Model\UserModel','user_id','loc_uid');
    }
}
