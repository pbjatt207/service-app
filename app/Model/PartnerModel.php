<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PartnerModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'partners';
    protected   $primaryKey     = 'partner_id';

    protected $casts = [
        'partner_uid' => 'integer',
    ];

    public function user() {
        return $this->hasOne('App\Model\UserModel', 'user_id', 'partner_uid');
    }
}
