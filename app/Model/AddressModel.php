<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AddressModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'user_addresses';
    protected   $primaryKey     = 'uaddr_id';

    protected $casts = [
        'uaddr_lat'     => 'string',
        'uadd_long'     => 'string',
        'uaddr_uid'     => 'integer',
    ];

    public function users() {
       return $this->belongsTo('App\Model\Order','uaddr_id','order_address');
    }
}
