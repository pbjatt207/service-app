<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StateModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'states';
    protected   $primaryKey     = 'state_id';

    protected $casts = [
        'state_country' => 'integer',
    ];
}
