<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubserviceModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'subservices';
    protected   $primaryKey     = 'subservice_id';

    protected $casts = [
        'subservice_sid'    => 'integer',
        'subservice_price'  => 'double',
    ];
}
