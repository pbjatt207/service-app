<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SliderModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'sliders';
    protected   $primaryKey     = 'slider_id';

    protected $casts = [
        'slider_sid' => 'integer',
    ];
}
