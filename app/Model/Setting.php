<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {
    public      $timestamps     = false;
    protected   $table          = 'settings';
    protected   $primaryKey     = 'setting_id';

    protected $casts = [
        'setting_refer_amount'              => 'integer',
        'setting_max_wallet_use'            => 'double',
        'setting_max_wallet_percent_use'    => 'double',
    ];
}
