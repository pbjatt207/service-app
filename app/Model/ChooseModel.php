<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ChooseModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'choose';
    protected   $primaryKey     = 'choose_id';
}
