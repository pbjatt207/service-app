<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model {
    public $timestamps          = false;
    protected   $table          = 'orders';
    protected   $primaryKey     = 'order_id';

    protected $casts = [
        'order_uid'         => 'integer',
        'order_address'     => 'integer',
        'order_total'       => 'double',
        'order_discount'    => 'double',
        'order_wallet_amt'  => 'double',
        'order_cid'         => 'integer',
        'order_vid'         => 'integer',
        'order_service_code'=> 'string',
        'order_happy_code'  => 'string',
        'order_time_end'    => 'integer',
    ];

    public function orderAddress() {
        return $this->belongsTo('App\Model\AddressModel', 'order_address', 'uaddr_id');
    }

    public function opro() {
        return $this->hasOne('App\Model\OrderProductModel', 'opro_oid', 'order_id');
    }

    public function user() {
        return $this->hasOne('App\Model\UserModel', 'user_id', 'order_uid');
    }

    public function partner() {
        return $this->hasOne('App\Model\UserModel', 'user_id', 'order_vid');
    }
}
