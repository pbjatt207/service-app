<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WalletModel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'wallets';
    protected   $primaryKey     = 'wallet_id';

    protected $casts = [
        'wallet_uid'    => 'integer',
        'wallet_amount' => 'double',
    ];

    public function user() {
        return $this->hasOne('App\Model\UserModel', 'user_id', 'wallet_uid');
    }
}
