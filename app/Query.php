<?php
namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

use App\Model\Jwt;

class Query extends Model {
    public function create_slug($title = "", $table_name, $field_name, $idField, $id) {
		$slug 		= preg_replace("/-$/","",preg_replace('/[^a-z0-9]+/i', "-", strtolower($title)));

		$row 		= DB::table($table_name)->select( [DB::raw('COUNT(*) as NumHits')] )->where($field_name, 'LIKE', "$slug%")->where($idField,'!=',$id)->first();
		$numHits 	= $row->NumHits;

		return ($numHits > 0) ? ($slug . '-' . $numHits) : $slug;
	}
    public static function push_to_apns($dev_token, $title, $body, $url_cnt = "https://www.google.com"){

        $authKey                  = url("AuthKey_CR7WLW45Z4.p8");
      	$arParam['teamId']        = 'RGPNHXZ96F';// Get it from Apple Developer's page
     	$arParam['authKeyId']     = 'CR7WLW45Z4';
      	$arParam['apns-topic']    = 'Kronos.HomeServices';
    	$arClaim                  = ['iss' => $arParam['teamId'], 'iat' => time()];
    	$arParam['p_key']         = file_get_contents($authKey);
    	$arParam['header_jwt']    = \App\Model\Jwt::encode($arClaim, $arParam['p_key'], $arParam['authKeyId'], 'RS256');

    	$arSendData               = array();

    	$arSendData['aps']['alert']['title']   = sprintf($title); // Notification title
    	$arSendData['aps']['alert']['body']    = sprintf($body); // body text
    	$arSendData['data']['jump-url']        = $url_cnt; // other parameters to send to the app

    	$sendDataJson = json_encode($arSendData);

    	$endPoint = 'https://api.development.push.apple.com/3/device'; // https://api.push.apple.com/3/device

    	//　Preparing request header for APNS
    	$ar_request_head[] = sprintf("content-type: application/json");
    	$ar_request_head[] = sprintf("authorization: bearer %s", $arParam['header_jwt']);
    	$ar_request_head[] = sprintf("apns-topic: %s", $arParam['apns-topic']);

    	// $dev_token = 'abcdefghijklmn123456';  // Device token

    	$url = sprintf("%s/%s", $endPoint, $dev_token);

    	$ch = curl_init($url);

    	curl_setopt($ch, CURLOPT_POSTFIELDS, $sendDataJson);
    	curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $ar_request_head);
    	$response = curl_exec($ch);
    	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    	if(empty(curl_error($ch))){
        // echo "empty curl error \n";
    	}

    	// Logging
      // After we need to remove device tokens which had response code 410.
      /*
    		if(intval($httpcode) == 200){ fwrite($fp_ok, $output); }
    		else{ fwrite($fp_ng, $output); }

    		if(intval($httpcode) == 410){ fwrite($fp_410, $output); }
      */
    	curl_close($ch);

    	return TRUE;
    }
}
