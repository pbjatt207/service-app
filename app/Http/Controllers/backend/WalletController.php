<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Query;
use App\Model\WalletModel as Wallet;
use App\Model\UserModel as User;

class WalletController extends BaseController {
    public function index(Request $request) {
        $q = new Query();

        if($request->isMethod('post')) {
		    $check = $request->input('check');
		    if(!empty($check)) {
		    	Wallet::whereIn('wallet_id', $check)->delete();
		    }

		    return redirect('service-panel/wallet');
    	}

        $users   = User::whereIn('user_role', ['serviceman', 'customer'])->get();
        $records = [];
        $wallet_uid = '';
        if(!empty($request->user_id)) {
            $wallet_uid = $request->user_id;
            $records = Wallet::with(['user'])->where('wallet_uid', $request->user_id)->paginate(100);
        }

        $title 	= "Wallet";
        $page 	= "wallet";
        $data 	= compact('page', 'title', 'records', 'users', 'wallet_uid');
        return view('backend/layout', $data);
    }

    public function add(Request $request) {
        $edit = array();
        if(!empty($id)) {
            $edit = Wallet::where('wallet_id', $id)->first();
        }

        if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {

	    		if(empty($edit->wallet_id)) {
		    		$id = Wallet::insertGetId($input);
		    	} else {
                    $id = $edit->wallet_id;
    		    	Wallet::where('wallet_id', $id)->update($input);
		    	}
                return redirect('service-panel/wallet?user_id='.$input['wallet_uid']);
		    }

    	}

        $users   = User::whereIn('user_role', ['serviceman', 'customer'])->get();
        $title 	= "Add Wallet";
        $page 	= "add_wallet";
        $data 	= compact('page', 'title', 'edit', 'users');
        return view('backend/layout', $data);
    }
}
