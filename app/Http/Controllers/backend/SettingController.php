<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\Setting;

class SettingController extends BaseController {
    public function index(Request $request) {
        $title 	= "Settings";
        $page 	= "settings";

        $edit = Setting::first();

        if($request->isMethod('post')) {
            $input = $request->input('record');

            Setting::whereRaw('1=1')->update($input);

            return redirect()->back()->with('success', 'information updated');
        }

        $data 	= compact('page', 'title', 'edit');
        return view('backend/layout', $data);
    }
}
