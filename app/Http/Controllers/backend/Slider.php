<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Model\SliderModel AS Smodel;
use App\Model\ServiceModel AS Service;

class Slider extends BaseController {
    public function index(Request $request) {
        $title 	= "View Sliders";
        $page 	     = "view_slider";

        $records = Smodel::leftJoin('services AS s', 'sliders.slider_sid', 's.service_id')->where('slider_is_deleted', 'N')->paginate(10);

        if($request->isMethod('post')) {
             $check = $request->input('check');
             if(!empty($check)) {
                  Smodel::whereIn('slider_id', $check)->update(['slider_is_deleted' => 'Y']);
             }

             return redirect('service-panel/slider');
        }

        $data 	= compact('page', 'title', 'records');
        return view('backend/layout', $data);
    }
    public function add(Request $request, $id = null) {
        $edit = [];
        if(!empty($id)) {
            $edit       = Smodel::where('slider_id', $id)->first();
        }

        $title 	= "Add Sliders";
        $page 	     = "add_slider";

        if($request->isMethod('post')) {
           $record   = $request->input('record');

           if(empty($id)) {
               Smodel::insert($record);
               $id = DB::getPdo()->lastInsertId();
           } else {
               Smodel::where('slider_id', $id)->update($record);
           }

           if ($request->hasFile('slider_image')) {
               if(!empty($edit->slider_image) && file_exists(public_path().'/imgs/sliders/'.$edit->slider_image)) {
                   unlink(public_path().'/imgs/sliders/'.$edit->slider_image);
               }
               $image           = $request->file('slider_image');
               $name            = 'IMG'.$id.'.'.$image->getClientOriginalExtension();
               $destinationPath = 'public/imgs/sliders';
               $image->move($destinationPath, $name);

               if(!empty($edit->slider_image)) {
                   $name .= "?v=".uniqid();
               }

               Smodel::where('slider_id', $id)->update( array('slider_image' => $name) );
           }

           return redirect('service-panel/slider')->with('success', "Success! New record inserted.");
        }

        $services = Service::where('service_is_deleted', 'N')->get();

        $data 	= compact('page', 'title', 'edit', 'services');
        return view('backend/layout', $data);
    }
}
