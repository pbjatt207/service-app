<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Query;
use App\Model\TimeslotModel AS Tmodel;
use DB;

class Timeslot extends BaseController {
    public function index(Request $request, $id = null) {
        $q = new Query();

        $edit = array();
        if(!empty($id)) {
            $edit = Tmodel::where('tslot_id', $id)->first();
        }

        if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {
	    		if(empty($edit->tslot_id)) {
		    		Tmodel::insert($input);
		    		$id = DB::getPdo()->lastInsertId();
		    	} else {
                    $id = $edit->tslot_id;
    		    	Tmodel::where('tslot_id', $id)->update($input);
		    	}
		    }

		    $check = $request->input('check');
		    if(!empty($check)) {
		    	Tmodel::whereIn('tslot_id', $check)->update(['tslot_is_deleted' => 'Y']);
		    }

		    return redirect('service-panel/timeslot');
    	}

    	$records = Tmodel::where('tslot_is_deleted', 'N')->paginate(10);

        $title 	= "Timeslots";
        $page 	= "timeslot";
        $data 	= compact('page', 'title', 'records', 'edit');
        return view('backend/layout', $data);
    }
}
