<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Model\CouponModel AS Cmodel;
use App\Model\ServiceModel AS Service;
use App\Model\UserModel AS User;
use App\Model\SubserviceModel AS Subservice;

class Coupon extends BaseController {
    public function index(Request $request) {
        $title 	= "View Promo Codes";
        $page 	= "view_coupon";

        if($request->isMethod('post')) {
             $check = $request->input('check');
             if(!empty($check)) {
                  Cmodel::whereIn('coupon_id', $check)->update(['coupon_is_deleted' => 'Y']);
             }

             return redirect('service-panel/coupon');
        }

        $records = Cmodel::where('coupon_is_deleted', 'N')->paginate(10);

        $data 	= compact('page', 'title', 'records');
        return view('backend/layout', $data);
    }
    public function add(Request $request, $id = null) {
        $edit = [];
        if(!empty($id)) {
            $edit       = Cmodel::where('coupon_id', $id)->first();
        }

        if($request->isMethod('post')) {
            $coupon   = $request->input('coupon');

            $isExistsQuery = Cmodel::where('coupon_code', 'LIKE', $coupon['coupon_code']);

            if(!empty($id)) {
                $isExistsQuery->where('coupon_id','!=',$id);
            }

            $isExists = $isExistsQuery->count();

            if(!$isExists) :
                $coupon['coupon_never_end']       = isset($coupon['coupon_never_end'])     ? $coupon['coupon_never_end']            : 0;
                $coupon['coupon_is_user_limit']   = isset($coupon['coupon_is_user_limit']) ? $coupon['coupon_is_user_limit']        : 0;
                $coupon['coupon_is_total_uses']   = isset($coupon['coupon_is_total_uses']) ? $coupon['coupon_is_total_uses']        : 0;
                $coupon['coupon_is_total_cart']   = isset($coupon['coupon_is_total_cart']) ? $coupon['coupon_is_total_cart']        : 0;
                $coupon['coupon_is_max_discount_amt'] = isset($coupon['coupon_is_max_discount_amt']) ? $coupon['coupon_is_max_discount_amt'] : 0;
                $coupon['coupon_uids']            = !empty($coupon['coupon_uids'])         ? implode(",", $coupon['coupon_uids'])   : '';

                if(empty($id)) {
                    Cmodel::insert($coupon);
                    $id = DB::getPdo()->lastInsertId();
                } else {
                    Cmodel::where('coupon_id', $id)->update($coupon);
                }

                if ($request->hasFile('coupon_image')) {
                    if(!empty($edit->coupon_image) && file_exists(public_path().'/imgs/coupons/'.$edit->coupon_image)) {
                        unlink(public_path().'/imgs/coupons/'.$edit->coupon_image);
                    }
                    $image           = $request->file('coupon_image');
                    $name            = 'IMG'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/imgs/coupons';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->coupon_image)) {
                        $name .= "?v=".uniqid();
                    }

                    Cmodel::where('coupon_id', $id)->update( array('coupon_image' => $name) );
                }

                return redirect('service-panel/coupon')->with('success', "Success! New record inserted.");

            else:
                return redirect()->back()->with('danger', "Failed! Coupon code already exists, please try another.");
            endif;
        }

        $subservices = [];
        $services = Service::where('service_is_deleted', 'N')->get();
        $users    = User::where('user_role', 'customer')->where('user_is_deleted', 'N')->get();

        if(!empty($edit->coupon_sid)) {
            $subservices = Subservice::where('subservice_is_deleted', 'N')->where('subservice_sid', $edit->coupon_sid)->get();
        }
        if($services->isEmpty()) {
            $services = [];
        }
        if($users->isEmpty()) {
            $users = [];
        }

        $title 	= "Add Promo Coupons";
        $page 	= "add_coupon";
        $data 	= compact('page', 'title', 'edit', 'services', 'subservices', 'users');
        return view('backend/layout', $data);
    }
}
