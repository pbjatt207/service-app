<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Query;
use App\Model\MilestoneModel as Milestone;

class MilestoneController extends BaseController {
    public function index(Request $request, $id = null) {
        $q = new Query();

        $edit = array();
        if(!empty($id)) {
            $edit = Milestone::where('ms_id', $id)->first();
        }

        if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {

	    		if(empty($edit->ms_id)) {
		    		$id = Milestone::insertGetId($input);
		    	} else {
                    $id = $edit->ms_id;
    		    	Milestone::where('ms_id', $id)->update($input);
		    	}
		    }

		    $check = $request->input('check');
		    if(!empty($check)) {
		    	Milestone::whereIn('ms_id', $check)->update(['ms_is_deleted' => 'Y']);
		    }

		    return redirect('service-panel/milestone');
    	}

    	$records = Milestone::where('ms_is_deleted', 'N')->paginate(10);

        $title 	= "Milestones";
        $page 	= "milestones";
        $data 	= compact('page', 'title', 'records', 'edit');
        return view('backend/layout', $data);
    }
}
