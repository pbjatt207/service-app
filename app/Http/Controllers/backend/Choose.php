<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Model\ChooseModel AS Cmodel;

class Choose extends BaseController {
    public function index(Request $request) {
        $title 	= "View Why Choose Us";
        $page 	= "view_choose";

        $records = Cmodel::where('choose_is_deleted', 'N')->paginate(10);

        if($request->isMethod('post')) {
             $check = $request->input('check');
             if(!empty($check)) {
                  Cmodel::whereIn('choose_id', $check)->update(['choose_is_deleted' => 'Y']);
             }

             return redirect('service-panel/slider');
        }

        $data 	= compact('page', 'title', 'records');
        return view('backend/layout', $data);
    }
    public function add(Request $request, $id = null) {
        $edit = [];
        if(!empty($id)) {
            $edit       = Cmodel::where('choose_id', $id)->first();
        }

        $title 	= "Add Why Choose Us";
        $page 	= "add_choose";

        if($request->isMethod('post')) {
           $record   = $request->input('record');

           if(empty($id)) {
               Cmodel::insert($record);
               $id = DB::getPdo()->lastInsertId();
           } else {
               Cmodel::where('choose_id', $id)->update($record);
           }

           if ($request->hasFile('choose_icon')) {
               if(!empty($edit->choose_icon) && file_exists(public_path().'/imgs/choose/'.$edit->choose_icon)) {
                   unlink(public_path().'/imgs/choose/'.$edit->choose_icon);
               }
               $image           = $request->file('choose_icon');
               $name            = 'ICO'.$id.'.'.$image->getClientOriginalExtension();
               $destinationPath = 'public/imgs/choose';
               $image->move($destinationPath, $name);

               if(!empty($edit->choose_icon)) {
                   $name .= "?v=".uniqid();
               }

               Cmodel::where('choose_id', $id)->update( array('choose_icon' => $name) );
           }

           return redirect('service-panel/choose')->with('success', "Success! New record inserted.");
        }

        $data 	= compact('page', 'title', 'edit');
        return view('backend/layout', $data);
    }
}
