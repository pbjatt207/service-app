<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Model\UserModel AS Umodel;
use App\Model\PartnerModel AS Partner;
use App\Model\CountryModel AS Country;
use App\Model\StateModel AS State;
use App\Model\CityModel AS City;
use App\Model\AddressModel AS Addr;
use App\Model\Location;

class User extends BaseController {
    public function index(Request $request, $role) {
        $title 	= ucwords($role)."";
        $page 	= "view_user";

        $role_name = ucwords($role);

        $records = Umodel::where('user_role', $role)->where('user_is_deleted', 'N')->paginate(10);

        $data 	= compact('page', 'title', 'records', 'role_name', 'role');
        return view('backend/layout', $data);
    }

    public function partner(Request $request) {
        $title 	= "Partners";
        $page 	= "view_partner";

        $records = Umodel::where('user_role', 'serviceman')->where('user_is_deleted', 'N')->paginate(10);

        $data 	= compact('page', 'title', 'records');
        return view('backend/layout', $data);
    }

    public function add(Request $request, $role, $id = null) {
        $edit = $states = $cities = $addresses = array();
        if(!empty($id)) {
            $edit       = Umodel::where('user_id', $id)->first();
            $addresses  = Addr::where('uaddr_uid', $id)->get();
        }

        $countries = Country::where('country_is_deleted', 'N')->get();
        if(!empty($edit->user_country)) {
            $states = State::where('state_is_deleted', 'N')->where('state_country', $edit->user_country)->get();
        }
        if(!empty($edit->user_state)) {
            $cities = City::where('city_is_deleted', 'N')->where('city_state', $edit->user_state)->get();
        }

        $title 	= "Add ".ucwords($role)." ";
        $page 	= "add_user";
        $role_name = ucwords($role);

        if($request->isMethod('post')) {
            $user   = $request->input('user');
            $addres = $request->input('addr');

            $isExistsQuery = Umodel::where('user_login', 'LIKE', $user['user_mobile']);

            if(!empty($id)) {
                $isExistsQuery->where('user_id','!=',$id);
            }

            $isExists = $isExistsQuery->count();

            if(!$isExists) :
                $user['user_role']      = $role;
                $user['user_login']     = $user['user_mobile'];
                $user['user_name']      = trim( $user['user_fname']." " .$user['user_lname'] );

                if(empty($id)) {
                    $user['user_created_on']    = date('Y-m-d H:i:s', time());

                    Umodel::insert($user);
                    $id = DB::getPdo()->lastInsertId();
                } else {
                    Umodel::where('user_id', $id)->update($user);
                }

                if(!empty($addres)) {
                    Addr::where('uaddr_uid', $id)->delete();
                    foreach($addres as $addr) {
                        if(!empty($addr['uaddr_address1']) || !empty($addr['uaddr_address2'])) {
                            $addr['uaddr_uid']  = $id;
                            $addr['uaddr_name'] = $user['user_name'];

                            Addr::insert($addr);
                        }
                    }
                }

                if ($request->hasFile('user_image')) {
                    if(!empty($edit->user_image) && file_exists(public_path().'/imgs/users/'.$edit->user_image)) {
                        unlink(public_path().'/imgs/users/'.$edit->user_image);
                    }
                    $image           = $request->file('user_image');
                    $name            = 'IMG'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/imgs/users';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->user_image)) {
                        $name .= "?v=".uniqid();
                    }

                    Umodel::where('user_id', $id)->update( array('user_image' => $name) );
                }

                return redirect('service-panel/user/'.$role)->with('success', "Success! New $role account has been created.");

            else:
                return redirect()->back()->with('danger', "Failed! Mobile no. already exists, please try another.");
            endif;
        }

        $data 	= compact('page', 'title', 'role_name', 'edit', 'role', 'countries', 'cities', 'states', 'addresses');
        return view('backend/layout', $data);
    }
    public function add_partner(Request $request, $id = null) {
        $edit = $states = $cities = array();
        if(!empty($id)) {
            $edit       = Umodel::where('user_id', $id)->first();
        }

        $countries = Country::where('country_is_deleted', 'N')->get();
        if(!empty($edit->user_country)) {
            $states = State::where('state_is_deleted', 'N')->where('state_country', $edit->user_country)->get();
        }
        if(!empty($edit->user_state)) {
            $cities = City::where('city_is_deleted', 'N')->where('city_state', $edit->user_state)->get();
        }

        $title 	= "Add Partner";
        $page 	= "add_partner";

        if($request->isMethod('post')) {
            $user    = $request->input('user');
            $partner = $request->input('partner');

            $isExistsQuery = Umodel::where('user_login', 'LIKE', $user['user_mobile'])->where('user_role', 'serviceman');

            if(!empty($id)) {
                $isExistsQuery->where('user_id','!=',$id);
            }

            $isExists = $isExistsQuery->count();

            if(!$isExists) :
                $user['user_role']      = 'serviceman';
                $user['user_login']     = $user['user_mobile'];
                $user['user_name']      = trim( $user['user_fname']." " .$user['user_lname'] );

                if(empty($id)) {
                    $user['user_created_on']    = date('Y-m-d H:i:s', time());
                    $id = Umodel::insertGetId($user);

                    $partner['partner_uid'] = $id;
                    Partner::insert( $partner );
                } else {
                    Umodel::where('user_id', $id)->update($user);
                    Partner::where('partner_uid', $id)->update( $partner );
                }

                if ($request->hasFile('user_image')) {
                    if(!empty($edit->user_image) && file_exists(public_path().'/imgs/users/'.$edit->user_image)) {
                        unlink(public_path().'/imgs/users/'.$edit->user_image);
                    }
                    $image           = $request->file('user_image');
                    $name            = 'IMG'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/imgs/users';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->user_image)) {
                        $name .= "?v=".uniqid();
                    }

                    Umodel::where('user_id', $id)->update( array('user_image' => $name) );
                }

                if ($request->hasFile('partner_uid_file')) {
                    if(!empty($edit->partner->partner_uid_file) && file_exists(public_path().'/uploads/users/'.$edit->partner->partner_uid_file)) {
                        unlink(public_path().'/uploads/users/'.$edit->partner->partner_uid_file);
                    }
                    $image           = $request->file('partner_uid_file');
                    $name            = 'UID'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/uploads/users';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->partner->partner_uid_file)) {
                        $name .= "?v=".uniqid();
                    }

                    Partner::where('partner_uid', $id)->update( ['partner_uid_file' => $name] );
                }

                if ($request->hasFile('partner_pan_file')) {
                    if(!empty($edit->partner->partner_pan_file) && file_exists(public_path().'/uploads/users/'.$edit->partner->partner_pan_file)) {
                        unlink(public_path().'/uploads/users/'.$edit->partner->partner_pan_file);
                    }
                    $image           = $request->file('partner_pan_file');
                    $name            = 'PAN'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/uploads/users';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->partner->partner_uid_file)) {
                        $name .= "?v=".uniqid();
                    }

                    Partner::where('partner_uid', $id)->update( ['partner_pan_file' => $name] );
                }

                if ($request->hasFile('partner_police_verification')) {
                    if(!empty($edit->partner->partner_police_verification) && file_exists(public_path().'/uploads/users/'.$edit->partner->partner_police_verification)) {
                        unlink(public_path().'/uploads/users/'.$edit->partner->partner_police_verification);
                    }
                    $image           = $request->file('partner_police_verification');
                    $name            = 'PV'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/uploads/users';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->partner->partner_police_verification)) {
                        $name .= "?v=".uniqid();
                    }

                    Partner::where('partner_uid', $id)->update( ['partner_police_verification' => $name] );
                }

                if ($request->hasFile('partner_id_file')) {
                    if(!empty($edit->partner->partner_id_file) && file_exists(public_path().'/uploads/users/'.$edit->partner->partner_id_file)) {
                        unlink(public_path().'/uploads/users/'.$edit->partner->partner_id_file);
                    }
                    $image           = $request->file('partner_id_file');
                    $name            = 'ID_Card_'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/uploads/users';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->partner->partner_id_file)) {
                        $name .= "?v=".uniqid();
                    }

                    Partner::where('partner_uid', $id)->update( ['partner_id_file' => $name] );
                }

                return redirect('service-panel/partner')->with('success', "Success! New partner account has been created.");

            else:
                return redirect()->back()->with('danger', "Failed! Mobile no. already exists, please try another.");
            endif;
        }

        $data 	= compact('page', 'title', 'edit', 'countries', 'cities', 'states');
        return view('backend/layout', $data);
    }
    public function location(Request $request, $uid = NULL) {
        $info       = Umodel::find($uid);

        $locations  = Location::where('loc_uid', $uid)->where('loc_date', date('Y-m-d', time()))->orderBy('loc_id')->get()->toArray();

        $data = compact('info', 'locations');
        return view('backend/inc/partner_locations', $data);
    }
    public function change_password(Request $request) {
        $title   = "Change Password";
        $page 	 = "change_password";

        $profile = DB::table('users')
            ->where('user_id', session('user_auth'))
            ->first();

        if($request->isMethod('post')) {
            $post = $request->input('record');

            if(password_verify($post['current_password'], $profile->user_password)) {
                $new_password = password_hash($post['new_password'], PASSWORD_DEFAULT);
                Umodel::where('user_id', $profile->user_id)->update( ['user_password' => $new_password] );

                return redirect()->back()->with('success', 'Success! Your password has been changed');
            } else {
                return redirect()->back()->with('danger', "Failed! Current password is not matched.");
            }
        }

        // $records = Umodel::where('user_role', $role)->where('user_is_deleted', 'N')->paginate(10);

        $data 	 = compact('page', 'title');
        return view('backend/layout', $data);
    }
}
