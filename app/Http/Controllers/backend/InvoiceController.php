<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Model\UserModel AS User;
use App\Model\AddressModel AS Addr;
use App\Model\OrderModel AS Order;
use App\Model\OrderProductModel AS OrderProduct;
use App\Model\Fpdf;

class InvoiceController extends BaseController {
    public function index(Request $request, $order_id = NULL) {
        $logo = 'public/imgs/logo.png';

        $order_info = Order::with(['user', 'opro', 'opro.service', 'orderAddress'])->find( $order_id );

        // dd($order_info);

        $pdf = new Fpdf();
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',18);
        $pdf->SetDrawColor(177, 173, 172);
        $pdf->Cell( 40, 40, $pdf->Image($logo, $pdf->GetX(), $pdf->GetY(), 0), 0, 0, 'L', false );
        $pdf->Cell( 100 , 6, '' );
        $pdf->Cell( 45, 6, 'Invoice #'.sprintf('%05d', $order_info->order_id), 0, 0, 'L', false );
        $pdf->Ln();
        $pdf->SetFont('Arial','',14);
        $pdf->Cell( 140, 6, '' );
        $pdf->Cell( 45, 6, 'Date '.date('d / m / Y'), 0, 0, 'L', false );
        $pdf->Ln(15);
        $pdf->SetFont('Arial','', 12);
        $userName = !empty($order_info->user->user_name) ? $order_info->user->user_name : "Customer";
        $pdf->Cell( 185, 6, 'Hi, '.$userName );
        $pdf->Ln();
        $pdf->Cell( 185, 6, 'Thank you for booking service with us' );
        $pdf->Ln(10);
        $pdf->SetFillColor(21,186,156);
        $pdf->SetTextColor(255,255,255);
        $pdf->SetFont('Arial','B', 16);
        $pdf->Cell( 2, 10, '', 0, 0, 'L', true );
        $pdf->Cell( 183, 10, 'Payment Summary', 0, 0, 'L', true );
        $pdf->Ln();
        $pdf->SetFont('Arial','', 12);
        $pdf->SetFillColor(247,247,247);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell( 2, 10, '', 0, 0, 'L', true );
        $pdf->Cell( 150, 10, 'Booking ID: '.sprintf('%05d', $order_info->order_id), 0, 0, 'L', true );
        $pdf->Cell( 33, 10, 'Payment Mode', 0, 0, 'L', true );
        $pdf->Ln();
        $pdf->Cell( 2, 10, '', 'B', 0, 'L', true );
        $pdf->Cell( 150, 10, $order_info->order_status, 'B', 0, 'L', true );
        $pdf->SetFont('Arial','B', 12);
        $pdf->Cell( 33, 10, $order_info->order_payment_type, 'B', 0, 'C', true );
        $pdf->Ln(10.2);
        $pdf->Cell( 2, 10, '', 0, 0, 'L', true );
        $pdf->Cell( 150, 10, $order_info->opro->service->service_name, 0, 0, 'L', true );
        $pdf->Cell( 31, 10, 'Rs. '.$order_info->opro->service->service_cost, 0, 0, 'R', true );
        $pdf->Cell( 2, 10, '', 0, 0, 'L', true );
        $pdf->Ln();
        $pdf->Cell( 2, 10, '', 'B', 0, 'L', true );
        $pdf->Cell( 150, 10, '', 'B', 0, 'L', true );
        $pdf->SetFont('Arial','', 12);
        $pdf->Cell( 31, 10, '(Per Hour)', 'B', 0, 'R', true );
        $pdf->Cell( 2, 10, '', 'B', 0, 'L', true );
        $pdf->Ln(10.5);
        $pdf->SetFont('Arial','', 12);
        $pdf->Cell( 2, 10, '', 0, 0, 'L', true );
        $pdf->Cell( 150, 10, 'Total Hours', 0, 0, 'L', true );
        $pdf->Cell( 31, 10, $order_info->opro->opro_qty.' Hrs.', 0, 0, 'R', true );
        $pdf->Cell( 2, 10, '', 0, 0, 'L', true );
        $pdf->Ln();
        $total = $subtotal = $order_info->order_total;
        $pdf->Cell( 2, 10, '', 0, 0, 'L', true );
        $pdf->Cell( 150, 10, 'Subtotal', 0, 0, 'L', true );
        $pdf->Cell( 31, 10, 'Rs. '.$subtotal, 0, 0, 'R', true );
        $pdf->Cell( 2, 10, '', 0, 0, 'L', true );
        $pdf->Ln();
        if(!empty($order_info->order_discount)) :
            $pdf->Cell( 2, 10, '', 0, 0, 'L', true );
            $pdf->Cell( 150, 10, 'Promo Discount', 0, 0, 'L', true );
            $pdf->Cell( 31, 10, '- Rs. '.$order_info->order_discount, 0, 0, 'R', true );
            $pdf->Cell( 2, 10, '', 0, 0, 'L', true );
            $pdf->Ln();

            $total -= $order_info->order_discount;
        endif;
        if(!empty($order_info->order_wallet_amt)) :
            $pdf->Cell( 2, 10, '', '', 0, 'L', true );
            $pdf->Cell( 150, 10, 'Wallet Discount', '', 0, 'L', true );
            $pdf->Cell( 31, 10, '- Rs. '.$order_info->order_wallet_amt, '', 0, 'R', true );
            $pdf->Cell( 2, 10, '', '', 0, 'L', true );
            $pdf->Ln(10.5);

            $total -= $order_info->order_wallet_amt;
        endif;
        $pdf->Cell( 2, 10, '', 'BT', 0, 'L', true );
        $pdf->Cell( 150, 10, 'Total Amount', 'BT', 0, 'L', true );
        $pdf->Cell( 31, 10, 'Rs. '.$total, 'BT', 0, 'R', true );
        $pdf->Cell( 2, 10, '', 'BT', 0, 'L', true );
        $pdf->Ln(10.5);
        $pdf->SetFont('Arial','', 10);
        $pdf->Cell( 185, 1, '', 0, 0, 'L', true );
        $pdf->Ln();
        $pdf->Cell( 2, 6, '', 0, 0, 'L', true );
        $pdf->Cell( 40, 6, 'Booking place on:', 0, 0, 'L', true );
        $pdf->SetFont('Arial','B', 10);
        $pdf->Cell( 143, 6, date('d M Y h:i A', strtotime($order_info->order_created_on)), 0, 0, 'L', true );
        $pdf->Ln();
        $pdf->SetFont('Arial','', 10);
        $pdf->Cell( 2, 6, '', 0, 0, 'L', true );
        $pdf->Cell( 40, 6, 'Service Duration:', 0, 0, 'L', true );
        $pdf->SetFont('Arial','B', 10);
        $pdf->Cell( 143, 6, $order_info->opro->opro_time.' Mins', 0, 0, 'L', true );
        $pdf->Ln();
        $pdf->SetFont('Arial','', 10);
        $pdf->Cell( 2, 6, '', 0, 0, 'L', true );
        $pdf->Cell( 183, 6, 'Service Address:', 0, 0, 'L', true );
        $pdf->Ln();
        $pdf->SetFont('Arial','B', 10);
        $pdf->Cell( 2, 6, '', 0, 0, 'L', true );
        $pdf->Cell( 183, 6, $order_info->orderAddress->uaddr_mobile, 0, 0, 'L', true );
        $pdf->Ln();
        $pdf->Cell( 2, 6, '', 0, 0, 'L', true );
        $pdf->Cell( 183, 6, $order_info->orderAddress->uaddr_address1, 0, 0, 'L', true );
        $pdf->Ln();

        if(!empty($order_info->orderAddress->uaddr_address2)) :
            $pdf->Cell( 2, 6, '', 0, 0, 'L', true );
            $pdf->Cell( 183, 6, $order_info->orderAddress->uaddr_address2, 0, 0, 'L', true );
            $pdf->Ln();
        endif;

        $pdf->Cell( 185, 6, ' ', 0, 0, 'L', true );
        $pdf->Output();
        exit;
    }
}
