<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class Dashboard extends BaseController {
   public function index() {
    	$title 		= "Dashboard";
    	$page = "dashboard";
    	$data = compact('page', 'title');
    	return view('backend/layout', $data);
    }
}
