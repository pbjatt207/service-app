<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Model\UserModel AS User;
use App\Model\AddressModel AS Addr;
use App\Model\OrderModel AS Omodel;

class Order extends BaseController {
    public function index(Request $request) {
        $title 	= "View Order";
        $page 	= "view_order";

        $query  = Omodel::with(['user','opro.service', 'partner'])->where('order_is_deleted', 'N');

        $status = "";
        if( !empty( $request->status ) ) {
            $status = $request->status;
            $query->where( 'order_status', $status );
        }
        $search = $request->input('search');
        if( !empty( $search['partner'] ) ) {
            $query->where( 'order_vid', $search['partner'] );
        }

        $records = $query->orderBy('order_id', 'DESC')->paginate(10);

        // dd($records);
        $partners = User::where('user_is_deleted', 'N')->where('user_role', 'serviceman')->get();

        if($request->isMethod('post')) {
             $post = $request->input();
             if(!empty($post['check']) && !empty($post['service_man_id'])) {
                 $serviceman = $post['service_man_id'];
                 Omodel::whereIn('order_id', $post['check'])->update(['order_vid' => $serviceman]);
                 return redirect()->back()->with('success', 'Partner assigned to order.');
             }
             if(!empty($post['check']) && !empty($post['order_status'])) {
                 $order_status = $post['order_status'];
                 Omodel::whereIn('order_id', $post['check'])->update(['order_status' => $order_status]);
                 return redirect()->back()->with('success', 'Order status has been changed.');
             }
        }

        $data 	= compact('page', 'title', 'records', 'partners', 'status', 'search');
        return view('backend/layout', $data);
    }
    public function customer(Request $request) {
          $title = "Select a customer";
          $page  = "order_customer";

          $s   = $request->input('search_keyword');
          $query = User::where('user_role', 'customer')->where('user_is_deleted', 'N');
          if(!empty($s)) {
               $query->where('user_name', 'LIKE', "%$s%");
          }
          $records = $query->paginate(12);

          $data 	= compact('page', 'title', 'records', 's');
          return view('backend/layout', $data);
    }
    public function add(Request $request, $id = null) {
        if(!empty($id)) {
            $edit       = Omodel::where('order_id', $id)->first();
        }

        $title 	= "Add Sliders";
        $page 	     = "add_slider";

        if($request->isMethod('post')) {
           $record   = $request->input('record');

           if(empty($id)) {
               Omodel::insert($record);
               $id = DB::getPdo()->lastInsertId();
           } else {
               Omodel::where('order_id', $id)->update($record);
           }

           if ($request->hasFile('order_image')) {
               if(!empty($edit->order_image) && file_exists(public_path().'/imgs/sliders/'.$edit->order_image)) {
                   unlink(public_path().'/imgs/sliders/'.$edit->order_image);
               }
               $image           = $request->file('order_image');
               $name            = 'IMG'.$id.'.'.$image->getClientOriginalExtension();
               $destinationPath = 'public/imgs/sliders';
               $image->move($destinationPath, $name);

               if(!empty($edit->order_image)) {
                   $name .= "?v=".uniqid();
               }

               Omodel::where('order_id', $id)->update( array('order_image' => $name) );
           }

           return redirect('service-panel/slider')->with('success', "Success! New record inserted.");
        }

        $data 	= compact('page', 'title', 'edit');
        return view('backend/layout', $data);
    }
}
