<?php

namespace App\Http\Controllers\backend;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Query;
use App\Model\SubserviceModel AS SSModel;

class Service extends BaseController {
    public function index(Request $request, $slug = null) {
        $q = new Query();

        $edit = array();
        if(!empty($slug)) {
            $edit = DB::table('services')->where('service_slug', $slug)->first();
        }

        if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {
                if(!empty($input['service_wish'])) $input['service_wish'] = serialize( $input['service_wish'] );

	    		if(empty($edit->service_id)) {
		    		DB::table('services')->insert($input);
		    		$id = DB::getPdo()->lastInsertId();
		    	} else {
                    $id = $edit->service_id;
    		    	DB::table('services')->where('service_id', $id)->update($input);
		    	}

                if ($request->hasFile('service_icon')) {
                    if(!empty($edit->service_icon) && file_exists(public_path().'/imgs/services/'.$edit->service_icon)) {
                        unlink(public_path().'/imgs/services/'.$edit->service_icon);
                    }
                    $image           = $request->file('service_icon');
                    $name            = 'MICO'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/imgs/services';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->service_icon)) {
                        $name .= "?v=".uniqid();
                    }

                    DB::table('services')->where('service_id', $id)->update( array('service_icon' => $name) );
                }

                if ($request->hasFile('service_image')) {
                    if(!empty($edit->service_image) && file_exists(public_path().'/imgs/services/'.$edit->service_image)) {
                        unlink(public_path().'/imgs/services/'.$edit->service_image);
                    }
                    $image           = $request->file('service_image');
                    $name            = 'MIMG'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/imgs/services';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->service_image)) {
                        $name .= "?v=".uniqid();
                    }

                    DB::table('services')->where('service_id', $id)->update( array('service_image' => $name) );
                }

                $slug = $q->create_slug($input['service_name'], "services", "service_slug", "service_id", $id);
                DB::table('services')->where('service_id', $id)->update( ['service_slug' => $slug] );
		    }

		    $check = $request->input('check');
		    if(!empty($check)) {
		    	DB::table('services')->whereIn('service_id', $check)->update(['service_is_deleted' => 'Y']);
		    }

		    return redirect('service-panel/service');
    	}

    	$records = DB::table('services')
    				->where('service_is_deleted', 'N')
    				->paginate(10);

        $title 	= "Services";
        $page 	= "services";
        $data 	= compact('page', 'title', 'records', 'edit');
        return view('backend/layout', $data);
    }
    public function add_subservice(Request $request, $slug = null) {
        $q = new Query();

        $edit = [];
        if(!empty($slug)) {
            $edit = SSModel::where('subservice_slug', $slug)->first();
        }

        if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {
                if(!empty($input['subservice_wish'])) $input['subservice_wish'] = serialize( $input['subservice_wish'] );

	    		if(empty($edit->subservice_id)) {
		    		SSModel::insert($input);
		    		$id = DB::getPdo()->lastInsertId();
		    	} else {
                    $id = $edit->subservice_id;
    		    	SSModel::where('subservice_id', $id)->update($input);
		    	}

                $slug = $q->create_slug($input['subservice_name'], "subservices", "subservice_slug", "subservice_id", $id);
                SSModel::where('subservice_id', $id)->update( ['subservice_slug' => $slug] );

                if ($request->hasFile('subservice_image')) {
                    if(!empty($edit->subservice_image) && file_exists(public_path().'/imgs/services/'.$edit->subservice_image)) {
                        unlink(public_path().'/imgs/services/'.$edit->subservice_image);
                    }
                    $image           = $request->file('subservice_image');
                    $name            = 'IMG'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/imgs/services';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->subservice_image)) {
                        $name .= "?v=".uniqid();
                    }

                    SSModel::where('subservice_id', $id)->update( array('subservice_image' => $name) );
                }
		    }

		    $check = $request->input('check');
		    if(!empty($check)) {
		    	SSModel::whereIn('subservice_id', $check)->update(['subservice_is_deleted' => 'Y']);
		    }

		    return redirect('service-panel/service/subservices');
    	}

        $services = DB::table('services')
    				->where('service_is_deleted', 'N')
    				->get();
        $title 	= "Add Type Of Service";
        $page 	= "add_subservice";
        $data 	= compact('page', 'title', 'edit', 'services');
        return view('backend/layout', $data);
    }
    public function subservices(Request $request) {
        if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {
	    		if(empty($edit->subservice_id)) {
		    		SSModel::insert($input);
		    		$id = DB::getPdo()->lastInsertId();
		    	} else {
                    $id = $edit->subservice_id;
    		    	SSModel::where('subservice_id', $id)->update($input);
		    	}

                $slug = $q->create_slug($input['subservice_name'], "subservices", "subservice_slug", "subservice_id", $id);
                SSModel::where('subservice_id', $id)->update( ['subservice_slug' => $slug] );
		    }

		    $check = $request->input('check');
		    if(!empty($check)) {
		    	SSModel::whereIn('subservice_id', $check)->update(['subservice_is_deleted' => 'Y']);
		    }

		    return redirect('service-panel/subservices');
    	}

        $records = SSModel::join('services', 'subservices.subservice_sid', 'services.service_id')->select('subservices.*', 'services.service_name')->where('subservice_is_deleted', 'N')->paginate(10);
        $title 	= "Add Sub-Service";
        $page 	= "view_subservices";
        $data 	= compact('page', 'title', 'records');
        return view('backend/layout', $data);
    }
}
