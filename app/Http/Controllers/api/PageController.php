<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\PageModel as Page;

class PageController extends BaseController {
    public function index(Request $request, $slug) {
        $page = Page::where('page_slug', $slug)->first();

        $re = [
            'status'    => TRUE,
            'data'      => $page
        ];

        return response()->json($re);
    }

    public function web(Request $request, $slug) {
        $page = Page::where('page_slug', $slug)->first();

        $data 	= compact('page');
        return view('api/page', $data);
    }
}
