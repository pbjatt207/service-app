<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\UserModel as Umodel;
use App\Model\AddressModel as Addr;
use App\Model\Location;
use App\Model\WalletModel as Wallet;
use App\Model\Setting;
use DB;

class User extends BaseController {
    public function sendotp(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];
        if(!empty($post['user_mobile'])) {
            $isExists = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source','user_refer_code')
                        ->where('user_is_deleted', 'N')
                        ->where('user_role', 'customer')
                        ->where('user_login', $post['user_mobile'])
                        ->first();

            $is_new = empty($isExists->user_id) ? 1 : 0;
            $otp    = rand(1000,9999);

            $mobile_no = $post['user_mobile'];
            $message   = urlencode("Your One time verification code is {$otp}.");

            file_get_contents("http://smshorizon.co.in/api/sendsms.php?user=Dipesh101&apikey=MtpftvFnUpJB4ZOf5FdU&mobile={$mobile_no}&message={$message}&senderid=xxyy&type=txt");

            $re = [
                'status'    => TRUE,
                'message'   => 'OTP sent to your mobile no.',
                'is_new'    => $is_new,
                'otp'       => $otp
            ];
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'Required field(s) is missing or appropriate data not found.'
            );
        }

        return response()->json($re);
    }
    public function login(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];
        if(!empty($post['user_mobile']) && !empty($post['user_source']) && in_array(@$post['user_source'], ['Android', 'Web', 'iPhone'])) {
            $isExists = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source','user_refer_code')
                        ->where('user_is_deleted', 'N')
                        ->where('user_role', 'customer')
                        ->where('user_login', $post['user_mobile'])
                        ->first();

            if(empty($isExists->user_id)) {
                $post['user_login'] = $post['user_mobile'];
                Umodel::insert($post);

                $isExists = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source','user_refer_code')
                            ->where('user_is_deleted', 'N')
                            ->where('user_login', $post['user_mobile'])
                            ->first();
            }

            if(!empty($post['refer_code'])) {
                Umodel::where('user_mobile', $mobile_no)->update(['user_refered_by' => $post['refer_code']]);
                $refer_person = Umodel::where('user_refered_by', $post['refer_code'])->first();

                $setting = Setting::first();

                if(!empty($refer_person->user_id)) {
                    Wallet::insert([
                        'wallet_uid'        => $refer_person->user_id,
                        'wallet_remarks'    => $mobile_no.' joined via refer code.',
                        'wallet_amount'     => $setting->setting_refer_amount,
                        'wallet_type'       => 'Credit'
                    ]);
                }
            }

            if(empty($isExists->user_refer_code)) {
                $string  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
                $code    = substr( str_shuffle($string), 0, 4 ).sprintf("%04d", $isExists->user_id);
                $code    = substr( $code, -8 );

                Umodel::where('user_id', $isExists->user_id)->update( ['user_refer_code' => $code] );
            }

            if(!empty($isExists->user_image)) {
                $isExists->user_image = url('imgs/users/'.$isExists->user_image);
            }
            if(!empty($isExists->user_dob) && strtotime($isExists->user_dob) > 0) {
                $isExists->user_dob = date("d/m/Y", strtotime($isExists->user_dob));
            } else {
                $isExists->user_dob = '';
            }

            $address_count  =  Addr::where('uaddr_uid', $isExists->user_id)->count();

            $re = array(
                'status'    => TRUE,
                'message'   => 'You\'re logged in.',
                'data'      => $isExists,
                'addr_count'=> $address_count
            );
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'Login failed! Required field(s) is missing or appropriate data not found.'
            );
        }

        return response()->json($re);
    }

    public function business_otp_send(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];
        if(!empty($post['user_mobile'])) {
            $isExists = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source','user_refer_code')
                        ->where('user_is_deleted', 'N')
                        ->where('user_role', 'serviceman')
                        ->where('user_login', $post['user_mobile'])
                        ->first();

            if(!empty($isExists->user_id)) {
                $is_new = empty($isExists->user_id) ? 1 : 0;
                $otp    = rand(1000,9999);

                $mobile_no = $post['user_mobile'];
                $message   = urlencode("Your One time verification code is {$otp}.");

                file_get_contents("http://smshorizon.co.in/api/sendsms.php?user=Dipesh101&apikey=MtpftvFnUpJB4ZOf5FdU&mobile={$mobile_no}&message={$message}&senderid=xxyy&type=txt");

                $re = [
                    'status'    => TRUE,
                    'message'   => 'OTP sent to your mobile no.',
                    'data'      => $isExists,
                    'otp'       => $otp
                ];
            } else {
                $re = [
                    'status'    => FALSE,
                    'message'   => 'Mobile no. not exists.'
                ];
            }
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'Required field(s) is missing or appropriate data not found.'
            );
        }

        return response()->json($re);
    }

    public function business_login(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];
        if(!empty($post['user_mobile']) && !empty($post['user_source']) && in_array(@$post['user_source'], ['Android', 'Web', 'iPhone'])) {
            $is_new = 0;

            $isExists = Umodel::select('user_id')
                        ->where('user_is_deleted', 'N')
                        ->where('user_role', 'serviceman')
                        ->where('user_login', $post['user_mobile'])
                        ->first();

            if(!empty($isExists->user_id)) {
                $profile = Umodel::with(['partner', 'country', 'state', 'city'])->find($isExists->user_id);

                $dateOfBirth    = date("Y-m-d", strtotime($profile->user_dob));
                $today          = date("Y-m-d");
                $diff           = date_diff(date_create($dateOfBirth), date_create($today));

                $profile->country_name   = $profile->country->country_name;
                $profile->state_name     = $profile->state->state_name;
                $profile->city_name      = $profile->city->city_name;
                $profile->age            = $diff->format('%y');

                $prefix                  = "SAP".substr( strtoupper( $profile->user_name ), 0, 2 ).'';
                $profile->employee_id    = sprintf('%s%03d', $prefix, $profile->user_id);

                $profile->user_image            = !empty($profile->user_image) ? url('imgs/users/'.$profile->user_image) : "";
                $profile->partner->partner_uid_file      = !empty($profile->partner->partner_uid_file) ? url('uploads/users/'.$profile->partner->partner_uid_file) : "";
                $profile->partner->partner_pan_file      = !empty($profile->partner->partner_pan_file) ? url('uploads/users/'.$profile->partner->partner_pan_file) : "";
                $profile->partner->partner_police_verification      = !empty($profile->partner->partner_police_verification) ? url('uploads/users/'.$profile->partner->partner_police_verification) : "";
                $profile->partner->partner_id_file      = !empty($profile->partner->partner_id_file) ? url('uploads/users/'.$profile->partner->partner_id_file) : "";

                $re = [
                    'status'    => TRUE,
                    'message'   => 'Login success!',
                    'data'      => $profile,
                ];
            } else {
                $re = [
                    'status'    => FALSE,
                    'message'   => 'Login failed! Mobile no. not exists.'
                ];
            }
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'Login failed! Required field(s) is missing or appropriate data not found.'
            );
        }

        return response()->json($re);
    }

    public function business_profile(Request $request, $user_id) {
        $profile = Umodel::with(['partner', 'country', 'state', 'city'])->find($user_id);

        $dateOfBirth    = date("Y-m-d", strtotime($profile->user_dob));
        $today          = date("Y-m-d");
        $diff           = date_diff(date_create($dateOfBirth), date_create($today));

        $profile->country_name   = $profile->country->country_name;
        $profile->state_name     = $profile->state->state_name;
        $profile->city_name      = $profile->city->city_name;
        $profile->age            = $diff->format('%y');

        $prefix                  = "SAP".substr( strtoupper( $profile->user_name ), 0, 2 ).'';
        $profile->employee_id    = sprintf('%s%03d', $prefix, $profile->user_id);

        $profile->user_image            = !empty($profile->user_image) ? url('imgs/users/'.$profile->user_image) : "";
        $profile->partner->partner_uid_file      = !empty($profile->partner->partner_uid_file) ? url('uploads/users/'.$profile->partner->partner_uid_file) : "";
        $profile->partner->partner_pan_file      = !empty($profile->partner->partner_pan_file) ? url('uploads/users/'.$profile->partner->partner_pan_file) : "";
        $profile->partner->partner_police_verification      = !empty($profile->partner->partner_police_verification) ? url('uploads/users/'.$profile->partner->partner_police_verification) : "";
        $profile->partner->partner_id_file      = !empty($profile->partner->partner_id_file) ? url('uploads/users/'.$profile->partner->partner_id_file) : "";

        $re = [
            'status'    => TRUE,
            'message'   => 'Profile found.',
            'data'      => $profile
        ];

        return response()->json($re);
    }

    public function refer(Request $request, $user_reff) {

        $isExists = Umodel::where('user_refer_code', $user_reff)->count();

        if($isExists) {

            $re = [
                'status'    => TRUE,
            ];

        } else {
            $re = [
                'status'    => FALSE,
            ];
        }

        return response()->json($re);
    }

    public function edit_profile(Request $request, $uid) {
        $post = $request->isMethod('post') ? $request->input() : [];
        $isExists = Umodel::select('user_id')
                    ->where('user_is_deleted', 'N')
                    ->where('user_id', $uid)
                    ->first();
        if(!empty($isExists->user_id)) {
            if(!empty($post['user_fname'])) {
                $post['user_name'] = trim( $post['user_fname']." ".@$post['user_lname'] );
            }
            Umodel::where('user_id', $uid)->update($post);
            $new = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source','user_refer_code')
                        ->where('user_is_deleted', 'N')
                        ->where('user_id', $uid)
                        ->first();

            $new->user_image = !empty($new->user_image) ? url('imgs/users/'.$new->user_image) : '';

            if(!empty($new->user_dob) && strtotime($new->user_dob) > 0) {
                $new->user_dob = date("d/m/Y", strtotime($new->user_dob));
            } else {
                $new->user_dob = '';
            }

            $re = array(
                'status'    => TRUE,
                'message'   => 'Profile updated successfully.',
                'data'      => $new
            );
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'ID not matched with any of profile.'
            );
        }

        return response()->json($re);
    }

    public function update_photo(Request $request) {
        $post = $request->input();
        if(!empty($post['user_id']) && $request->hasFile('user_image')) {
            $id = $post['user_id'];

            $edit = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source','user_refer_code')
                        ->where('user_is_deleted', 'N')
                        ->where('user_id', $id)
                        ->first();

            if(!empty($edit->user_image) && file_exists(public_path().'/imgs/users/'.$edit->user_image)) {
                unlink(public_path().'/imgs/users/'.$edit->user_image);
            }
            $image           = $request->file('user_image');
            $name            = 'IMG'.$id.'.'.$image->getClientOriginalExtension();
            $destinationPath = 'public/imgs/users';
            $image->move($destinationPath, $name);

            if(!empty($edit->user_image)) {
                $name .= "?v=".uniqid();
            }

            Umodel::where('user_id', $id)->update( array('user_image' => $name) );

            $new = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source', 'user_refer_code')
                        ->where('user_is_deleted', 'N')
                        ->where('user_id', $id)
                        ->first();

            $new->user_image = !empty($new->user_image) ? url('imgs/users/'.$new->user_image) : '';

            $re = array(
                'status'    => TRUE,
                'message'   => 'Image updated',
                'data'      => $new
            );
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing'
            ];
        }

        return response()->json($re);
    }

    public function add_address(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];

        if(!empty($post['uaddr_address1']) && !empty($post['uaddr_address2']) && !empty($post['uaddr_lat']) && !empty($post['uadd_long']) && !empty($post['uaddr_uid'])) {
            if(empty($post['uaddr_id'])) {
                Addr::insert($post);
                $re = [
                    'status'    => TRUE,
                    'message'   => 'New address added.'
                ];
            } else {
                Addr::where('uaddr_id', $post['uaddr_id'])->update($post);
                $re = [
                    'status'    => TRUE,
                    'message'   => 'address updated.'
                ];
            }

        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json($re);
    }

    public function remove_address($addr_id) {
        $address = Addr::where('uaddr_id', $addr_id)->update(['uaddr_is_deleted' => 'Y']);
        $re = [
            'status'    => TRUE,
            'message'   => 'address removed.'
        ];

        return response()->json($re);
    }

    public function view_address(Request $request, $uid) {
        $addresses = Addr::where('uaddr_uid', $uid)->where('uaddr_is_deleted', 'N')->orderBy('uaddr_id', 'DESC')->get();

        if(!$addresses->isEmpty()) {
            $re = [
                'status'    => TRUE,
                'message'   => 'Records found.',
                'data'      => $addresses
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'No addresses added yet.'
            ];
        }

        return response()->json($re);
    }

    public function save_location(Request $request) {
        $post = $request->input();

        if(!empty($post['loc_lat']) && !empty($post['loc_long']) && !empty($post['loc_uid'])) {
            $post['loc_date'] = date('Y-m-d', time());
            Location::insert( $post );

            $re = [
                'status'    => TRUE,
                'message'   => 'Location saved'
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json( $re );
    }
}
