<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\ServiceModel as Service;
use App\Model\SubserviceModel as Subservice;
use App\Model\CartModel as Cart;
use App\Model\SliderModel as Slider;
use App\Model\ChooseModel as Choose;
use App\Model\AddressModel as Addr;
use App\Model\OrderModel as Order;
use DB;

class ServiceController extends BaseController {
    public function index(Request $request) {
        $services       = Service::where('service_is_deleted', 'N')->get();
        $sliders        = Slider::where('slider_is_deleted', 'N')->get();

        if(!$services->isEmpty()) {
            foreach ($services as $key => $s) {
                if(!empty($s->service_image)) {
                    $services[$key]->service_image = url('imgs/services/'.$s->service_image);
                }
                if(!empty($s->service_icon)) {
                    $services[$key]->service_icon = url('imgs/services/'.$s->service_icon);
                }
                if(!empty($s->service_wish)) {
                    $services[$key]->service_wish = unserialize($s->service_wish);
                }
            }
        } else {
            $services = [];
        }

        if(!$sliders->isEmpty()) {
            foreach ($sliders as $key => $s) {
                if(!empty($s->slider_image)) {
                    $sliders[$key]->slider_image = url('imgs/sliders/'.$s->slider_image);
                }
                $sliders[$key]->slider_sid = intval( $s->slider_sid );
            }
        } else {
            $sliders = [];
        }

        $re = [
            'status'    => TRUE,
            'message'   => 'record(s) found.',
            'data'      => $services,
            'sliders'   => $sliders
        ];

        return response()->json($re);
    }
    public function subservices(Request $request, $sid, $uid = NULL) {
        $service        = Service::where('service_is_deleted', 'N')->where('service_id', $sid)->first();
        $chooses        = Choose::where('choose_is_deleted', 'N')->get();

        $chooses        = $chooses->isEmpty() ? [] : $chooses;
        $address_count  =  Addr::where('uaddr_uid', $uid)->count();
        if(!empty($service->service_image)) {
            $service->service_image = url('imgs/services/'.$service->service_image);
        }
        if(!empty($service->service_icon)) {
            $service->service_icon = url('imgs/services/'.$service->service_icon);
        }
        if(!empty($service->service_wish)) {
            $service->service_wish = unserialize($service->service_wish);
        }
        if(!$chooses->isEmpty()) {
            foreach ($chooses as $key => $s) {
                if(!empty($s->choose_icon)) {
                    $chooses[$key]->choose_icon = url('imgs/choose/'.$s->choose_icon);
                }
            }
        } else {
            $chooses = [];
        }
        if(!empty($service->service_id)) {
            $subservices = Subservice::where('subservice_is_deleted', 'N')->where('subservice_sid', $sid)->get();

            if(!$subservices->isEmpty()) {
                foreach ($subservices as $key => $s) {
                    if(!empty($s->subservice_image)) {
                        $subservices[$key]->subservice_image = url('imgs/services/'.$s->subservice_image);
                    }
                    if(!empty($s->subservice_wish)) {
                        $subservices[$key]->subservice_wish = unserialize( $s->subservice_wish );
                    }
                    $flag = 0;
                    if(!empty($uid)) {
                        $isCardAdded = Cart::where('cart_sid', $s->subservice_id)->where('cart_uid', $uid)->count();
                        $flag        = $isCardAdded;
                    }
                    $subservices[$key]->in_cart = $flag;
                }

                $due_order = [];
                $due = 0;

                if(!empty($uid)) {
                    $due     = Order::where('order_uid', $uid)->where('order_status', 'Deny')->where('order_is_paid', 'N')->where('order_is_deleted', 'N')->sum('order_total');
                    if($due) {
                        $due_order = Order::where('order_uid', $uid)->where('order_status', 'Deny')->where('order_is_paid', 'N')->where('order_is_deleted', 'N')->select('order_total', 'order_wallet_amt', 'order_discount', 'coupon_type')->first();

                        $due_order->order_subtotal  = (!empty($due_order->coupon_type) && $due_order->coupon_type == "Discount") ? $due_order->order_total + $due_order->order_discount : $due_order->order_total;
                        $due_order->order_subtotal += $due_order->order_wallet_amt;
                    }
                }

                $re = [
                    'status'            => TRUE,
                    'message'           => 'record(s) found.',
                    'address_count'     => $address_count,
                    'data'              => $subservices,
                    'service'           => $service,
                    'why_choose'        => $chooses,
                    'dumAmt'            => $due,
                    'due_order'         => $due_order
                ];
            } else {
                $re = [
                    'status'    => FALSE,
                    'message'   => 'Empty record(s) found.'
                ];
            }
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'No service found related to given ID.'
            ];
        }

        return response()->json($re);
    }
}
