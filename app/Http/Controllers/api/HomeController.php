<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\MilestoneModel as Milestone;
use App\Model\WalletModel as Wallet;
use DB;

class HomeController extends BaseController {
    public function index(Request $request, $user_id) {
        $milestones = Milestone::get();
        $total_credit = Wallet::where('wallet_uid', $user_id)->where('wallet_type', 'Credit')->sum('wallet_amount');
        $total_debit  = Wallet::where('wallet_uid', $user_id)->where('wallet_type', 'Debit')->sum('wallet_amount');

        // $walletAmt  = Wallet::selectRaw('SUM(`wallet_credit`) AS total_credit, SUM(`wallet_debit`) AS total_debit')->where('wallet_uid', $user_id)->first();

        // $walletAmt  = $walletAmt->total_credit - $walletAmt->total_debit;

        $walletAmt = $total_credit - $total_debit;

        $re = [
            'milestones'    => $milestones,
            'wallet_amt'    => $walletAmt
        ];

        return response()->json($re);
    }
    public function login(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];
        if(!empty($post['user_mobile']) && !empty($post['user_source']) && in_array(@$post['user_source'], ['Android', 'Web', 'iPhone'])) {
            $isExists = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source','user_refer_code')
                        ->where('user_is_deleted', 'N')
                        ->where('user_role', 'customer')
                        ->where('user_login', $post['user_mobile'])
                        ->first();
            if(empty($isExists->user_id)) {
                $post['user_login'] = $post['user_mobile'];
                Umodel::insert($post);

                $isExists = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source','user_refer_code')
                            ->where('user_is_deleted', 'N')
                            ->where('user_login', $post['user_mobile'])
                            ->first();
            }

            if(empty($isExists->user_refer_code)) {
                $string  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
                $code    = substr( str_shuffle($string), 0, 4 ).sprintf("%04d", $isExists->user_id);
                $code    = substr( $code, -8 );

                Umodel::where('user_id', $isExists->user_id)->update( ['user_refer_code' => $code] );
            }

            if(!empty($isExists->user_image)) {
                $isExists->user_image = url('imgs/users/'.$isExists->user_image);
            }
            if(!empty($isExists->user_dob) && strtotime($isExists->user_dob) > 0) {
                $isExists->user_dob = date("d/m/Y", strtotime($isExists->user_dob));
            } else {
                $isExists->user_dob = '';
            }

            $address_count  =  Addr::where('uaddr_uid', $isExists->user_id)->count();

            $re = array(
                'status'    => TRUE,
                'message'   => 'You\'re logged in.',
                'data'      => $isExists,
                'addr_count'=> $address_count
            );
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'Login failed! Required field(s) is missing or appropriate data not found.'
            );
        }

        return response()->json($re);
    }

    public function business_otp_send(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];
        if(!empty($post['user_mobile'])) {
            $isExists = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source','user_refer_code')
                        ->where('user_is_deleted', 'N')
                        ->where('user_role', 'serviceman')
                        ->where('user_login', $post['user_mobile'])
                        ->first();

            if(!empty($isExists->user_id)) {
                $is_new = empty($isExists->user_id) ? 1 : 0;
                $otp    = rand(1000,9999);

                $mobile_no = $post['user_mobile'];
                $message   = urlencode("Your One time verification code is {$otp}.");

                file_get_contents("http://smshorizon.co.in/api/sendsms.php?user=Dipesh101&apikey=MtpftvFnUpJB4ZOf5FdU&mobile={$mobile_no}&message={$message}&senderid=xxyy&type=txt");

                $re = [
                    'status'    => TRUE,
                    'message'   => 'OTP sent to your mobile no.',
                    'data'      => $isExists,
                    'otp'       => $otp
                ];
            } else {
                $re = [
                    'status'    => FALSE,
                    'message'   => 'Mobile no. not exists.'
                ];
            }
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'Required field(s) is missing or appropriate data not found.'
            );
        }

        return response()->json($re);
    }

    public function business_login() {
        $post = $request->isMethod('post') ? $request->input() : [];
        if(!empty($post['user_mobile']) && !empty($post['user_source']) && in_array(@$post['user_source'], ['Android', 'Web', 'iPhone'])) {
            $is_new = 0;

            $isExists = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source','user_refer_code')
                        ->where('user_is_deleted', 'N')
                        ->where('user_role', 'serviceman')
                        ->where('user_login', $post['user_mobile'])
                        ->first();

            if(!empty($isExists->user_id)) {
                $re = [
                    'status'    => TRUE,
                    'message'   => 'Login success!',
                    'data'      => $isExists,
                ];
            } else {
                $re = [
                    'status'    => FALSE,
                    'message'   => 'Login failed! Mobile no. not exists.'
                ];
            }
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'Login failed! Required field(s) is missing or appropriate data not found.'
            );
        }
    }

    public function refer(Request $request, $uid, $user_reff) {

        $isExists = Umodel::where('user_refer_code', $user_reff)->count();

        if($isExists) {

            Umodel::where('user_id', $uid)->update(['user_refered_by' => $user_reff]);

            $re = [
                'status'    => TRUE,
                'message'   => 'success'
            ];

        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Refer code is not valid.'
            ];
        }

        return response()->json($re);
    }

    public function edit_profile(Request $request, $uid) {
        $post = $request->isMethod('post') ? $request->input() : [];
        $isExists = Umodel::select('user_id')
                    ->where('user_is_deleted', 'N')
                    ->where('user_id', $uid)
                    ->first();
        if(!empty($isExists->user_id)) {
            if(!empty($post['user_fname'])) {
                $post['user_name'] = trim( $post['user_fname']." ".@$post['user_lname'] );
            }
            Umodel::where('user_id', $uid)->update($post);
            $new = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source','user_refer_code')
                        ->where('user_is_deleted', 'N')
                        ->where('user_id', $uid)
                        ->first();

            $new->user_image = !empty($new->user_image) ? url('imgs/users/'.$new->user_image) : '';

            if(!empty($new->user_dob) && strtotime($new->user_dob) > 0) {
                $new->user_dob = date("d/m/Y", strtotime($new->user_dob));
            } else {
                $new->user_dob = '';
            }

            $re = array(
                'status'    => TRUE,
                'message'   => 'Profile updated successfully.',
                'data'      => $new
            );
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'ID not matched with any of profile.'
            );
        }

        return response()->json($re);
    }

    public function update_photo(Request $request) {
        $post = $request->input();
        if(!empty($post['user_id']) && $request->hasFile('user_image')) {
            $id = $post['user_id'];

            $edit = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source','user_refer_code')
                        ->where('user_is_deleted', 'N')
                        ->where('user_id', $id)
                        ->first();

            if(!empty($edit->user_image) && file_exists(public_path().'/imgs/users/'.$edit->user_image)) {
                unlink(public_path().'/imgs/users/'.$edit->user_image);
            }
            $image           = $request->file('user_image');
            $name            = 'IMG'.$id.'.'.$image->getClientOriginalExtension();
            $destinationPath = 'public/imgs/users';
            $image->move($destinationPath, $name);

            if(!empty($edit->user_image)) {
                $name .= "?v=".uniqid();
            }

            Umodel::where('user_id', $id)->update( array('user_image' => $name) );

            $new = Umodel::select('user_id', 'user_fname', 'user_lname', 'user_name', 'user_image', 'user_dob', 'user_address1', 'user_address2', 'user_mobile', 'user_email', 'user_source', 'user_refer_code')
                        ->where('user_is_deleted', 'N')
                        ->where('user_id', $id)
                        ->first();

            $new->user_image = !empty($new->user_image) ? url('imgs/users/'.$new->user_image) : '';

            $re = array(
                'status'    => TRUE,
                'message'   => 'Image updated',
                'data'      => $new
            );
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing'
            ];
        }

        return response()->json($re);
    }

    public function add_address(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];

        if(!empty($post['uaddr_address1']) && !empty($post['uaddr_address2']) && !empty($post['uaddr_lat']) && !empty($post['uadd_long']) && !empty($post['uaddr_uid'])) {
            if(empty($post['uaddr_id'])) {
                Addr::insert($post);
                $re = [
                    'status'    => TRUE,
                    'message'   => 'New address added.'
                ];
            } else {
                Addr::where('uaddr_id', $post['uaddr_id'])->update($post);
                $re = [
                    'status'    => TRUE,
                    'message'   => 'address updated.'
                ];
            }

        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json($re);
    }

    public function remove_address($addr_id) {
        $address = Addr::where('uaddr_id', $addr_id)->delete();
        $re = [
            'status'    => TRUE,
            'message'   => 'address removed.'
        ];

        return response()->json($re);
    }

    public function view_address(Request $request, $uid) {
        $addresses = Addr::where('uaddr_uid', $uid)->get();

        if(!$addresses->isEmpty()) {
            $re = [
                'status'    => TRUE,
                'message'   => 'Records found.',
                'data'      => $addresses
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'No addresses added yet.'
            ];
        }

        return response()->json($re);
    }
}
