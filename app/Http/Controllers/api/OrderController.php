<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\OrderModel as Order;
use App\Model\OrderProductModel as OrderProduct;
use App\Model\UserModel as User;
use App\Model\CartModel as Cart;
use App\Model\WalletModel as Wallet;
use DB;

class OrderController extends BaseController {
    public function add(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];

        if(!empty($post['order_uid']) && !empty($post['order_address']) && !empty($post['order_total']) && !empty($post['order_schedule_date']) && !empty($post['order_schedule_time'])) {
            $post['order_created_on'] = date('Y-m-d H:i:s', time());

            Order::insert( $post );

            $id     = DB::getPdo()->lastInsertId();
            $carts  = Cart::where('cart_uid', $post['order_uid'])->get();

            foreach($carts as $cart) {
                $arr = [
                    'opro_sid'          => $cart->cart_sid,
                    'opro_qty'          => 1,
                    'opro_oid'          => $id,
                    'opro_time'         => '0',
                    'opro_start_time'   => '00:00:00',
                    'opro_end_time'     => '00:00:00'
                ];
                OrderProduct::insert($arr);
            }

            $order = Order::join('user_addresses AS uaddr', 'orders.order_address', 'uaddr.uaddr_id')
                        ->join('users AS u', 'orders.order_uid', 'u.user_id')
                        ->leftJoin('coupons AS c', 'orders.order_cid', 'c.coupon_id')
                        ->where('order_id', $id)->first();

            // $order->

            $products = OrderProduct::join('services AS s', 's.service_id', 'order_products.opro_sid')
                            ->where('opro_oid', $id)
                            ->get();

            foreach($products as $k => $p) {
                $products[$k]->service_image    = !empty($p->service_image) ? url('imgs/services/'.$p->service_image) : '';
                $products[$k]->service_icon     = !empty($p->service_icon) ? url('imgs/services/'.$p->service_icon) : '';

                $products[$k]->service_cost     = doubleval( $p->service_cost );
            }

            $order->products = $products;

            $service_code = rand(1000, 9999);
            $order->order_service_code = $service_code;

            $order->order_subtotal  = (!empty($order->coupon_type) && $order->coupon_type == "Discount") ? $order->order_total + $order->order_discount : $order->order_total;
            $order->order_subtotal += $order->order_wallet_amt;

            Order::where('order_id', $id)->update(['order_service_code' => $service_code]);

            if(!empty($post['order_wallet_amt'])) {
                Wallet::insert([
                    'wallet_uid'        => $post['order_uid'],
                    'wallet_remarks'    => 'Order No. '.sprintf("%06d", $id).' placed.',
                    'wallet_amount'     => $post['order_wallet_amt'],
                    'wallet_type'       => 'Debit',
                    'wallet_created_on' => date('Y-m-d H:i:s', time())
                ]);
            }

            $re = [
                'status'    => TRUE,
                'message'   => 'order has been placed.',
                'order_id'  => $id,
                'order_info'=> $order
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json($re);
    }
    public function edit(Request $request, $order_id) {
        $post = $request->input();
        if(!empty($post['order_schedule_date']) && !empty($post['order_schedule_time'])) {
            Order::where('order_id', $order_id)->update($post);

            $re = [
                'status'    => TRUE,
                'message'   => 'Order updated.'
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json( $re );
    }
    public function edit_order_cart(Request $request, $endservice = 0) {
        $post = $request->input();

        if(!empty($post['order_id'])) {
            $post['service_time'] = !empty($post['service_time']) ? $post['service_time'] : 0;
            Order::where('order_id', $post['order_id'])->update(['order_status' => 'In Progress']);

            $qty  = floor( $post['service_time'] / 60 ) > 0 ? floor( $post['service_time'] / 60 ) : 1;
            $qty .= $post['service_time'] % 60 > 0 && $qty >= 1 ? ".5" : "";

            if( $post['service_time'] % 60 > 30 && $qty >= 1 ) {
                $qty = ceil( $post['service_time'] / 60 );
            }

            $arr = [
                'opro_qty'  => $qty,
                'opro_time' => $post['service_time']
            ];

            if(!empty($post['start_time'])) {
                $arr['opro_start_time'] = $post['start_time'];
            }

            if(!empty($post['end_time'])) {
                $arr['opro_end_time'] = $post['end_time'];
            }

            $order_info = Order::
                join('order_products AS op', 'orders.order_id', 'op.opro_oid')
                ->where('order_id', $post['order_id'])
                ->join('services AS s', 'op.opro_sid', 's.service_id')
                ->leftJoin('coupons AS c', 'c.coupon_id', 'orders.order_cid')
                ->first();

            OrderProduct::where('opro_oid', $post['order_id'])->update($arr);

            $orderPro = OrderProduct::join('services AS s', 'order_products.opro_sid', 's.service_id')->where('opro_oid', $post['order_id'])->first();
            $amount   = $orderPro->service_cost * $orderPro->opro_qty;

            $discount = 0;
            if(!empty($order_info->order_cid)) {
                if($order_info->coupon_discount_type == "Percent") {
                    $discount = $amount * $order_info->coupon_discount / 100;
                } else {
                    $discount = $order_info->coupon_discount;
                }
            }

            $amount -= $discount;
            $amount -= $order_info->order_wallet_amt;

            Order::where('order_id', $post['order_id'])->update(['order_total' => $amount, 'order_discount' => $discount]);

            $re = [
                'status'    => TRUE,
                'message'   => "Data updated"
            ];

            if($endservice) {

                Order::where('order_id', $post['order_id'])->update([
                    'order_time_end'    => 1
                ]);

                $order_info = Order::
                    join('order_products AS op', 'orders.order_id', 'op.opro_oid')
                    ->where('order_id', $post['order_id'])
                    ->join('services AS s', 'op.opro_sid', 's.service_id')
                    ->leftJoin('coupons AS c', 'c.coupon_id', 'orders.order_cid')
                    ->first();

                $order_info->order_subtotal  = (!empty($order_info->coupon_type) && $order_info->coupon_type == "Discount") ? $order_info->order_total + $order_info->order_discount : $order_info->order_total;
                $order_info->order_subtotal += $order_info->order_wallet_amt;

                // $re['happy_code'] = $happy_code;
                $re['order_info'] = $order_info;
            }
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json( $re );
    }
    public function pending(Request $request, $type = "pending") {
        $input = $request->input();

        if(!empty($input['order_id'])) {
            if($type == "pending") {
                Order::where('order_id', $input['order_id'])->update(['order_status' => 'Pending']);
                if(!empty($input['service_time']) && !empty($input['end_time'])) {
                    OrderProduct::where('opro_oid', $input['order_id'])->update(['opro_time' => $input['service_time'], 'opro_end_time' => $input['end_time']]);
                }
                $re = [
                    'status'    => TRUE,
                    'message'   => 'Order moved to pending.'
                ];
            } elseif($type == "resume") {
                Order::where('order_id', $input['order_id'])->update(['order_status' => 'In Progress']);
                $re = [
                    'status'    => TRUE,
                    'message'   => 'Order resumed back.'
                ];
            } else {
                $re = [
                    'status'    => FALSE,
                    'message'   => 'Incorrect type.'
                ];
            }
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json( $re );
    }

    public function deny(Request $request) {
        $input = $request->input();

        if(!empty($input['order_id']) && !empty($input['deny_reason'])) {
            Order::where('order_id', $input['order_id'])->update( ['order_status' => 'Deny', 'order_deny_reason' => $input['deny_reason']] );

            $re = [
                'status'    => TRUE,
                'message'   => 'Order denied successfully.'
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json( $re );
    }

    public function cancel(Request $request, $order_id) {
        $reason = $request->input('cancel_reason');

        if(!empty($order_id) && !empty($reason)) :

            Order::where('order_id', $order_id)->update(['order_status' => 'Cancelled', 'order_cancel_reason' => $reason]);

            $re = [
                'status'    => TRUE,
                'message'   => 'Order cancelled.'
            ];

        else:
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        endif;

        return response()->json( $re );
    }
    public function completed(Request $request, $order_id, $payment_mode, $txn_id = "") {
        $isExists = Order::where('order_id', $order_id)->count();

        if($isExists) :
            $happy_code = rand(1000, 9999);

            $arr = [
                'order_status'       => 'Completed',
                'order_happy_code'   => $happy_code,
                'order_payment_type' => $payment_mode,
                'order_txn_id'       => $txn_id
            ];
            Order::where('order_id', $order_id)->update($arr);

            $re = [
                'status'    => TRUE,
                'happy_code'=> $happy_code,
                'message'   => 'Order completed marked.'
            ];

        else:
            $re = [
                'status'    => FALSE,
                'message'   => 'Order ID is not correct.'
            ];
        endif;

        return response()->json( $re );
    }
    public function fill_happy_code($order_id, $happy_code) {
        $is_correct = Order::where('order_id', $order_id)->where('order_happy_code', $happy_code)->count();
        if($is_correct) {
            Order::where('order_id', $order_id)->update(['order_is_happy' => 'Y']);
            $re = [
                'status'    => TRUE,
                'message'   => 'Happy code is matched.'
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Happy code is not matched.'
            ];
        }

        return response()->json( $re );
    }
    public function happy_codes(Request $request, $user_id) {
        $orders = Order::join('order_products AS op', 'orders.order_id', 'op.opro_oid')->join('services AS s', 'op.opro_sid', 's.service_id')->where('order_uid', $user_id)->where('order_is_happy', 'N')->select('order_id', 'order_happy_code', 'order_service_code', 'service_name')->orderBy('order_id', 'DESC')->get();

        if(!$orders->isEmpty()) {
            $re = [
                'status'    => TRUE,
                'message'   => $orders->count()." record(s) found.",
                'data'      => $orders
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'No record(s) found.'
            ];
        }

        return response()->json( $re );
    }
    public function my_order(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];
        if(!empty($post['user_id'])) {
            $orders = Order::leftJoin('coupons AS c', 'orders.order_cid', 'c.coupon_id')->where('order_uid', $post['user_id'])->orderBy('order_id', 'DESC')->get();

            if(!$orders->isEmpty()) {
                foreach($orders as $key => $o) {
                    $products = OrderProduct::join('services AS s', 's.service_id', 'order_products.opro_sid')
                                    ->where('opro_oid', $o->order_id)
                                    ->get();

                    foreach($products as $k => $p) {
                        $products[$k]->service_image    = !empty($p->service_image) ? url('imgs/services/'.$p->service_image) : '';
                        $products[$k]->service_icon     = !empty($p->service_icon) ? url('imgs/services/'.$p->service_icon) : '';
                    }

                    $orders[$key]->services           = $products;
                    $orders[$key]->order_created_date = date('d-m-Y', strtotime( $o->order_created_on ));
                    $orders[$key]->order_created_time = date('h:i A', strtotime( $o->order_created_on ));

                    $orders[$key]->order_subtotal     = (!empty($o->coupon_type) && $o->coupon_type == "Discount") ? $o->order_total + $o->order_discount : $o->order_total;
                    $orders[$key]->order_subtotal     += $o->order_wallet_amt;
                }
                $re = [
                    'status'    => TRUE,
                    'message'   => $orders->count().' record(s) found.',
                    'data'      => $orders
                ];
            } else {
                $re = [
                    'status'    => FALSE,
                    'message'   => 'No record(s) found.'
                ];
            }


        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }
        return response()->json($re);
    }
    public function order_single(Request $request, $order_id) {
        $order = Order::join('user_addresses AS uaddr', 'orders.order_address', 'uaddr.uaddr_id')
                        ->leftJoin('users AS u', 'orders.order_vid', 'u.user_id')
                        ->leftJoin('partners AS p', 'u.user_id', 'p.partner_uid')
                        ->leftJoin('coupons AS c', 'c.coupon_id', 'orders.order_cid')
                        ->leftJoin('timeslots AS t', 't.tslot_name', 'orders.order_schedule_time')
                        ->where('order_id', $order_id)->first();

        if(!empty($order->order_id)) {
            $order->order_uid               = intval( $order->order_uid );
            $order->order_address           = intval( $order->order_address );
            $order->order_total             = doubleval( $order->order_total );
            $order->order_discount          = doubleval( $order->order_discount );
            $order->order_wallet_amt        = doubleval( $order->order_wallet_amt );
            $order->order_cid               = intval( $order->order_cid );
            $order->order_vid               = intval( $order->order_vid );
            $order->order_time_end          = intval( $order->order_time_end );

            $order->partner_uid             = intval( $order->partner_uid );

            $order->user_state              = intval( $order->user_state );
            $order->user_city               = intval( $order->user_city );
            $order->user_pincode            = intval( $order->user_pincode );

            $order->coupon_never_end        = intval( $order->coupon_never_end );
            $order->coupon_is_user_limit    = intval( $order->coupon_is_user_limit );
            $order->coupon_limit_no         = doubleval( $order->coupon_limit_no );
            $order->coupon_is_total_uses    = intval( $order->coupon_is_total_uses );
            $order->coupon_limit_no         = doubleval( $order->coupon_limit_no );
            $order->coupon_is_total_uses    = intval( $order->coupon_is_total_uses );
            $order->coupon_total_uses       = doubleval( $order->coupon_total_uses );
            $order->coupon_is_total_cart    = intval( $order->coupon_is_total_cart );
            $order->coupon_min_cart_total   = doubleval( $order->coupon_min_cart_total );
            $order->coupon_is_max_discount_amt  = intval( $order->coupon_is_max_discount_amt );
            $order->coupon_max_discount_amt = doubleval( $order->coupon_max_discount_amt );
            $order->coupon_sid              = intval( $order->coupon_sid );
            $order->coupon_ssid             = intval( $order->coupon_ssid );

            $order->tslot_min_time          = intval( $order->tslot_min_time );
        }

        // $order = Order::with(['orderAddress', 'partner', 'partner.partner'])->find($order_id);

        if(!empty($order->user_image)) {
            $order->user_image   = url('imgs/users/'.$order->user_image);
        }

        if(!empty($order->partner_uid_file)) {
            $order->partner_uid_file   = url('uploads/users/'.$order->partner_uid_file);
        }

        if(!empty($order->partner_pan_file)) {
            $order->partner_pan_file   = url('uploads/users/'.$order->partner_pan_file);
        }

        if(!empty($order->partner_police_verification)) {
            $order->partner_police_verification   = url('uploads/users/'.$order->partner_police_verification);
        }

        if(!empty($order->partner_id_file)) {
            $order->partner_id_file   = url('uploads/users/'.$order->partner_id_file);
        }

        $order->receipt_url  = 'https://www.tutorialspoint.com/ios/ios_tutorial.pdf';

        $order->subtotal     = (!empty($order->coupon_type) && $order->coupon_type == "Discount") ? $order->order_total + $order->order_discount : $order->order_total;
        $order->subtotal    += $order->order_wallet_amt;

        unset($order->partner);

        $prefix                = "SAP".substr( strtoupper( $order->user_name ), 0, 2 ).'';
        $order->employee_id    = sprintf('%s%03d', $prefix, $order->user_id);

        $products              = OrderProduct::join('services AS s', 's.service_id', 'order_products.opro_sid')
                        ->where('opro_oid', $order_id)
                        ->get();

        foreach($products as $k => $p) {
            $products[$k]->service_order_limit  = intval($p->service_order_limit);

            $products[$k]->service_cost         = doubleval($p->service_cost);
            $products[$k]->service_order_limit  = intval($p->service_order_limit);

            $products[$k]->service_image        = !empty($p->service_image) ? url('imgs/services/'.$p->service_image) : '';
            $products[$k]->service_icon         = !empty($p->service_icon) ? url('imgs/services/'.$p->service_icon) : '';
        }

        $re = [
            'status'    => TRUE,
            'data'      => $order,
            'products'  => $products
        ];

        // var_dump($re); die;
        return response()->json($re);
    }
}
