<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Query;
use App\Model\Jwt;

class NotificationController extends BaseController {
    public function send(Request $request) {

        $dev_token = $request->input('dev_token');

        $title = "A testing Title - Service App";
        $body  = "Push Notification Testing Message";

    	$stat = Query::push_to_apns($dev_token, $title, $body);
    	if($stat == FALSE){
            exit();
    	} else {
            $re = [
                'status'    => TRUE,
                'message'   => 'Push Notification sent to Apple.'
            ];
        }

        return response()->json($re);
    }
}
