<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\WalletModel as Wallet;
use App\Model\Setting;

class WalletController extends BaseController {
    public function index(Request $request, $user_id) {
        $records = Wallet::where('wallet_uid', $user_id)->orderBy('wallet_id', 'DESC')->get();

        if(!$records->isEmpty()) {
            $balance = 0;
            foreach($records as $key => $rec) {
                if($rec->wallet_type == "Credit")
                    $balance += $rec->wallet_amount;
                else
                    $balance -= $rec->wallet_amount;

                $records[$key]->balance = $balance;
                $records[$key]->wallet_created_on = date("d/m/Y h:i A", strtotime($rec->wallet_created_on));
            }

            $setting = Setting::first()->toArray();
            $re = [
                'status'        => TRUE,
                'message'       => $records->count().' record(s) found.',
                'data'          => $records,
                'wallet_amount' => $balance
            ];

            $re = array_merge($re, $setting);
        }
        else {
            $re = [
                'status'    => FALSE,
                'message'   => 'No record(s) found.'
            ];
        }

        return response()->json($re);
    }
}
