<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\TimeslotModel AS Tslot;
use App\Model\OrderModel as Order;
use App\Model\ServiceModel as Service;
use DB;

class ScheduleController extends BaseController {
    public function index(Request $request) {
        $current_hour = date('H', time());
        $current_date = date('d', time());

        $timeslots      = Tslot::where('tslot_min_time', '>', $current_hour)->where('tslot_is_deleted', 'N')->get();
        $alltimeslots   = Tslot::get();
        $dates          = [];

        // $current_date = $timeslots->isEmpty() ? date('y-m-d', time());
        $start_count = $timeslots->isEmpty() ? 1 : 0;
        for($i = $start_count; $i <= $start_count + 9; $i++) {
            $dates[] = [
                'month'     => date('M', strtotime('+'.$i.' days')),
                'date'      => date('d', strtotime('+'.$i.' days')),
                'day'       => date('D', strtotime('+'.$i.' days')),
                'fulldate'  => date('Y-m-d', strtotime('+'.$i.' days'))
            ];
        }

        if($timeslots->isEmpty()) {
            $timeslots = $alltimeslots;
        }

        $re = [
            'status'        => true,
            'timeslots'     => $timeslots,
            'dates'         => $dates,
            'alltimeslots'  => $alltimeslots
        ];

        return response()->json($re);
    }
    public function date_timeslotes(Request $request, $sid = NULL) {
        $service_info = Service::find($sid);

        $current_hour = date('H', time());
        $current_date = date('d', time());

        $timeslots      = Tslot::where('tslot_min_time', '>', $current_hour)->where('tslot_is_deleted', 'N')->orderBy('tslot_min_time')->get();
        $alltimeslots   = Tslot::where('tslot_is_deleted', 'N')->orderBy('tslot_min_time')->get();
        $dates          = [];

        // $current_date = $timeslots->isEmpty() ? date('y-m-d', time());
        $start_count = $timeslots->isEmpty() ? 1 : 0;
        for($i = $start_count; $i <= $start_count + 9; $i++) {
            $time_string = strtotime('+'.$i.' days');
            if( date('Y-m-d', time()) == date('Y-m-d', $time_string) ) {
                $timeslotArr = $timeslots;
            } else {
                $timeslotArr = $alltimeslots;
            }

            foreach($timeslotArr as $key => $ts) {
                $order_count = Order::join('order_products AS op', 'orders.order_id', 'op.opro_oid')
                ->where('order_is_deleted', 'N')
                ->where('order_schedule_date', date('d M D', $time_string))
                ->where('order_schedule_time', $ts->tslot_name)
                ->where('opro_sid', $sid)
                ->count();
                $disabled = "N";
                if($order_count > $service_info->service_order_limit) {
                    $disabled = "Y";
                }

                $timeslotArr[$key]->is_disabled = $disabled;
            }

            $dates[] = [
                'month'     => date('M', $time_string),
                'date'      => date('d', $time_string),
                'day'       => date('D', $time_string),
                'fulldate'  => date('Y-m-d', $time_string),
                'timeslots' => $timeslotArr
            ];
        }

        if($timeslots->isEmpty()) {
            $timeslots = $alltimeslots;
        }

        $re = [
            'status'    => true,
            'data'      => $dates
        ];

        return response()->json($re);
    }
}
