<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\UserModel as User;
use App\Model\SliderModel as Slider;
use App\Model\ChooseModel as Choose;
use App\Model\AddressModel as Addr;
use App\Model\Setting;
use App\Model\PageModel as Page;
use DB;

class StartController extends BaseController {
    public function index(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];
        if(!empty($post['user_id']) && !empty($post['user_device_id']) && !empty($post['user_source']) && in_array(@$post['user_source'], ['Android', 'Web', 'iPhone'])) {
            $id  = $post['user_id'];
            unset( $post['user_id'] );

            User::where('user_id', $id)->update($post);

            $profile = User::where('user_id',$id)->first();

            $profile->user_image = !empty($profile->user_image) ? url('imgs/users/'.$profile->user_image) : '';

            if(!empty($profile->user_dob) && strtotime($profile->user_dob) > 0) {
                $profile->user_dob = date("d/m/Y", strtotime($profile->user_dob));
            } else {
                $profile->user_dob = '';
            }

            $setting = Setting::first()->toArray();

            $setting['setting_payment_info'] = explode(PHP_EOL, str_replace("\r", "", $setting['setting_payment_info']) );
            $setting['setting_cancel_reason'] = explode(PHP_EOL, str_replace("\r", "", $setting['setting_cancel_reason']) );
            $setting['setting_deny_reasons'] = explode(PHP_EOL, str_replace("\r", "", $setting['setting_deny_reasons']) );

            $pages   = Page::whereIn('page_slug', ['about-us', 'our-mission', 'our-vision'])->select('page_title', 'page_description')->get();

            foreach ($pages as $key => $page) {
                $pages[$key]->page_description = strip_tags( html_entity_decode( $page->page_description ) );
            }

            $re = [
                'status'        => TRUE,
                'message'       => 'record updated.',
                'profile'       => $profile,
                'refer_string'  => $setting['setting_refer_string'],
                'about_us'      => $pages
            ];


            $re = array_merge($re, $setting);
            $re['setting_refer_amount']             = intval( $re['setting_refer_amount'] );
            $re['setting_max_wallet_use']           = floatval( $re['setting_max_wallet_use'] );
            $re['setting_max_wallet_percent_use']   = floatval( $re['setting_max_wallet_percent_use'] );
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json($re);
    }

    public function business(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];
        if(!empty($post['user_id']) && !empty($post['user_device_id']) && !empty($post['user_source']) && in_array(@$post['user_source'], ['Android', 'Web', 'iPhone'])) {
            $id  = $post['user_id'];
            unset( $post['user_id'] );

            User::where('user_id', $id)->update($post);

            $profile = User::with(['partner', 'country', 'state', 'city'])->find($id);

            $dateOfBirth    = date("Y-m-d", strtotime($profile->user_dob));
            $today          = date("Y-m-d");
            $diff           = date_diff(date_create($dateOfBirth), date_create($today));

            $profile->country_name   = $profile->country->country_name;
            $profile->state_name     = $profile->state->state_name;
            $profile->city_name      = $profile->city->city_name;
            $profile->age            = $diff->format('%y');

            $prefix                  = "SAP".substr( strtoupper( $profile->user_name ), 0, 2 ).'';
            $profile->employee_id    = sprintf('%s%03d', $prefix, $profile->user_id);

            $profile->user_image            = !empty($profile->user_image) ? url('imgs/users/'.$profile->user_image) : "";
            $profile->partner->partner_uid_file      = !empty($profile->partner->partner_uid_file) ? url('uploads/users/'.$profile->partner->partner_uid_file) : "";
            $profile->partner->partner_pan_file      = !empty($profile->partner->partner_pan_file) ? url('uploads/users/'.$profile->partner->partner_pan_file) : "";
            $profile->partner->partner_police_verification      = !empty($profile->partner->partner_police_verification) ? url('uploads/users/'.$profile->partner->partner_police_verification) : "";
            $profile->partner->partner_id_file      = !empty($profile->partner->partner_id_file) ? url('uploads/users/'.$profile->partner->partner_id_file) : "";

            if(!empty($profile->user_dob) && strtotime($profile->user_dob) > 0) {
                $profile->user_dob = date("d/m/Y", strtotime($profile->user_dob));
            } else {
                $profile->user_dob = '';
            }

            $setting = Setting::first()->toArray();
            $setting['setting_payment_info'] = explode(PHP_EOL, str_replace("\r", "", $setting['setting_payment_info']) );
            $setting['setting_cancel_reason'] = explode(PHP_EOL, str_replace("\r", "", $setting['setting_cancel_reason']) );
            $setting['setting_deny_reasons'] = explode(PHP_EOL, str_replace("\r", "", $setting['setting_deny_reasons']) );

            $re = [
                'status'        => TRUE,
                'message'       => 'record updated.',
                'profile'       => $profile,
                'refer_string'  => $setting['setting_refer_string']
            ];

            $re = array_merge($re, $setting);
            $re['setting_refer_amount']             = intval( $re['setting_refer_amount'] );
            $re['setting_max_wallet_use']           = floatval( $re['setting_max_wallet_use'] );
            $re['setting_max_wallet_percent_use']   = floatval( $re['setting_max_wallet_percent_use'] );
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json($re);
    }
}
