<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\OrderModel as Order;
use App\Model\CartModel as Cart;
use App\Model\AddressModel as Addr;
use App\Model\WalletModel as Wallet;
use App\Model\Setting;
use DB;

class CartController extends BaseController {
    public function add(Request $request, $cart_uid, $cart_sid) {
        // $is_exists = Cart::where('cart_uid', $cart_uid)->where('cart_sid', $cart_sid)->count();

        Cart::where('cart_uid', $cart_uid)->delete();

        // if(!$is_exists) {
            Cart::insert(compact('cart_uid', 'cart_sid'));

            $re = [
                'status'    => TRUE,
                'message'   => 'Service added to cart.'
            ];
        // }
        // else {
        //     Cart::where('cart_uid', $cart_uid)->where('cart_sid', $cart_sid)->delete();
        //     $re = [
        //         'status'    => TRUE,
        //         'message'   => 'Service removed from cart.'
        //     ];
        // }

        return response()->json($re);
    }
    public function index(Request $request, $cart_uid) {
        $carts = Cart::join('services as s', 'carts.cart_sid', 's.service_id')
            ->where('cart_uid', $cart_uid)
            ->get();

        $address_count  =  Addr::where('uaddr_uid', $cart_uid)->count();
        if(!$carts->isEmpty()) {
            $total_credit = Wallet::where('wallet_uid', $cart_uid)->where('wallet_type', 'Credit')->sum('wallet_amount');
            $total_debit  = Wallet::where('wallet_uid', $cart_uid)->where('wallet_type', 'Debit')->sum('wallet_amount');
            $wallet_amt   = $total_credit - $total_debit;

            foreach ($carts as $key => $c) {
                // if(!empty($c->subservice_image)) {
                //     $carts[$key]->subservice_image = url('imgs/services/'.$c->subservice_image);
                // }
                // if(!empty($c->subservice_wish)) {
                //     $carts[$key]->subservice_wish = unserialize( $c->subservice_wish );
                // }
                $carts[$key]->service_id            = intval($c->service_id);
                $carts[$key]->service_cost          = doubleval($c->service_cost);
                $carts[$key]->service_order_limit   = intval($c->service_order_limit);
                if(!empty($c->service_image)) {
                    $carts[$key]->service_image = url('imgs/services/'.$c->service_image);
                }
            }

            $setting = Setting::select('setting_max_wallet_use', 'setting_max_wallet_percent_use')->first()->toArray();
            $due     = Order::where('order_uid', $cart_uid)->where('order_status', 'Pending')->where('order_is_paid', 'N')->where('order_is_deleted', 'N')->sum('order_total');

            $re = [
                'status'        => TRUE,
                'message'       => 'Cart services found.',
                'data'          => $carts,
                'wallet_amount' => $wallet_amt,
                'address_count' => $address_count,
                'dueAmt'        => $due
            ];

            $re = array_merge($re, $setting);

            $re['setting_max_wallet_use']           = floatval( $re['setting_max_wallet_use'] );
            $re['setting_max_wallet_percent_use']   = floatval( $re['setting_max_wallet_percent_use'] );
        } else {
            $re = [
                'status'        => FALSE,
                'message'       => 'Cart is empty',
                'address_count' => $address_count
            ];
        }

        return response()->json($re);
    }
}
