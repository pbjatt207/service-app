@php
$locArr = $waypoints = [];
if(!empty($locations)) {
    $i = 0;
    foreach($locations as $loc) { $i++;
        // $latlong  = trim($loc['loc_lat'].", ".$loc['loc_long']);
        $locArr[] = [
            "Stop ".$i,
            $loc['loc_lat'],
            $loc['loc_long'],
            $i
        ];
    }
}
$locJSON    = json_encode($locArr, JSON_PRETTY_PRINT);
@endphp
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>{{ $info->user_name }} Live Routes</title>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <link href="https://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <!-- <div id="directions" style="width: 100%;height:500px;float:left"></div> -->
        <div id="map_canvas" style="width: 100%;height: 100vh;"></div>
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyABsqp57nbpOgVf4DIhaL8vXCVj3JQIXUQ"></script>
        <script type="text/javascript">
            var geocoder;
            var map;
            var directionsDisplay;
            var directionsService = new google.maps.DirectionsService();
            var locations = <?php echo $locJSON ?>;

            function initialize() {
                directionsDisplay = new google.maps.DirectionsRenderer();


                var map = new google.maps.Map(document.getElementById('map_canvas'), {
                  zoom: 10,
                  center: new google.maps.LatLng(-33.92, 151.25),
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                directionsDisplay.setMap(map);
                var infowindow = new google.maps.InfoWindow();

                var marker, i;
                var request = {
                  travelMode: google.maps.TravelMode.DRIVING
                };
                for (i = 0; i < locations.length; i++) {
                  marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                  });

                  google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                      infowindow.setContent(locations[i][0]);
                      infowindow.open(map, marker);
                    }
                  })(marker, i));

                  if (i == 0) request.origin = marker.getPosition();
                  else if (i == locations.length - 1) request.destination = marker.getPosition();
                  else {
                    if (!request.waypoints) request.waypoints = [];
                    request.waypoints.push({
                      location: marker.getPosition(),
                      stopover: true
                    });
                  }

                }
                directionsService.route(request, function(result, status) {
                  if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(result);
                  }
                });
            }
            google.maps.event.addDomListener(window, "load", initialize);
        </script>
    </body>
</html>
