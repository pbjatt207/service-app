<section class="page-header mb-3">
    <div class="container-fluid subservice">
        <div class="clearfix">
            <div class="float-left">
                <h1>{{ !empty($edit->user_id) ? "Edit" : "Add" }} {{ $role_name }}</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href=""> <i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ url('user/'.$role) }}">{{ $role_name }}</a></li>
                <li class="breadcrumb-item active">{{ !empty($edit->user_id) ? "Edit" : "Add" }} {{ $role_name }}</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post" enctype="multipart/form-data" autocomplete="off">
            <input autocomplete="off" name="hidden" type="search" style="display:none;">
            @csrf
            @if (\Session::has('danger'))
                <div class="alert alert-danger">
                    {!! \Session::get('danger') !!}</li>
                </div>
            @endif
            <a href="#save-data" class="float-right"> <i class="icon-save"></i> {{ !empty($edit->user_id) ? "Update" : "Save" }} </a>
        	<h3 class="card-title">
        		<div class="mr-auto">Login Details</div>
        	</h3>
        	<div class="row">
        		<div class="col-sm-8 col-lg-9">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>First Name (Required)</label>
                    			<input type="text" name="user[user_fname]" value="{{ @$edit->user_fname }}" placeholder="First Name" class="form-control" required autocomplete="new_name">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Last Name</label>
                    			<input type="text" name="user[user_lname]" value="{{ @$edit->user_lname }}" placeholder="Last Name" class="form-control" autocomplete="new_name">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Date Of Birth</label>
                                <input type="text" name="user[user_dob]" value="{{ @$edit->user_dob }}" class="form-control datepicker_dob" placeholder="yyyy-mm-dd" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Mobile No. (Required)</label>
                    			<input type="tel" name="user[user_mobile]" value="{{ @$edit->user_mobile }}" placeholder="Mobile No." class="form-control" required autocomplete="new_mobile">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Email ID</label>
                    			<input type="email" name="user[user_email]" value="{{ @$edit->user_email }}" placeholder="Email ID" class="form-control" autocomplete="new_email">
                            </div>
                        </div>
                    </div>
                    <div id="addressRows">
                        @php $i = 0; @endphp
                        @if(empty($addresses))
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label>House / Flat No.</label>
                                        <input type="text" name="addr[0][uaddr_address1]" value="" placeholder="House / Flat No." class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label>Location (Required)</label>
                                        <input type="text" name="addr[0][uaddr_address2]" value="" placeholder="Location" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="d-none d-sm-block d-md-block d-lg-block">&nbsp;</label>
                                        <button type="button" class="btn btn-success btn-block add_addr_row"> <i class="icon-plus-circle"></i> </button>
                                    </div>
                                </div>
                            </div>
                        @else
                            @foreach($addresses as $addr)
                                <input type="hidden" name="addr[{{ $i }}][uaddr_id]" value="{{ $addr->uaddr_id }}">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label>House / Flat No.</label>
                                            <input type="text" name="addr[{{ $i }}][uaddr_address1]" value="{{ $addr->uaddr_address1 }}" placeholder="House / Flat No." class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label>Location (Required)</label>
                                            <input type="text" name="addr[{{ $i }}][uaddr_address2]" value="{{ $addr->uaddr_address2 }}" placeholder="Location" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="d-none d-sm-block d-md-block d-lg-block">&nbsp;</label>
                                            <button type="button" class="btn btn-block {{ $i == 0 ? 'add_addr_row btn-success' : 'remove_addr_row btn-danger' }}"> <i class="{{ $i == 0 ? 'icon-plus-circle' : 'icon-minus-circle' }}"></i> </button>
                                        </div>
                                    </div>
                                </div>
                                @php $i++; @endphp
                            @endforeach
                        @endif
                        <input type="hidden" id="totalAddRow" value="{{ $i - 1 }}">
                    </div>
        		</div>
                <div class="col-sm-4 col-lg-3">
                    <label class="upload_image">
                        <img src="{{ empty($edit->user_image) ? url('imgs/no-image.png') : url('imgs/users/'.$edit->user_image) }}" alt="Upload Image" title="Upload Image">
                        <input type="file" name="user_image" accept="image/*" id="userImage">
                    </label>
                    <label for="userImage" class="btn btn-primary btn-block">Choose File</label>
                </div>
        	</div>
        </form>
    </div>
</div>
