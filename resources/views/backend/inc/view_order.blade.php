<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View Orders</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href="">Dashboard</a></li>
                <li class="breadcrumb-item active">View Orders</li>
            </ul>
        </div>
    </div>
</section>
<div>
        <div class="card mb-3">
            {{ Form::open(['method' => 'get']) }}
            <input type="hidden" name="status" value="{{ $status }}">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Search By Partner</label>
                        <select class="form-control" name="search[partner]">
                            <option value="">Select User</option>
                            @foreach($partners as $u)
                            <option value="{{ $u->user_id }}" @if(@$search['partner'] == $u->user_id) selected @endif>{{ $u->user_name.' '.$u->user_mobile }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-primary btn-block">Search</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
<div class="card">
    <form method="post">
        @csrf
        <div class="float-right" style="width: 150px;">
            <select class="form-control" name="service_man_id" id="service_man_id">
                <option value="">Select Service Man</option>
                @foreach($partners as $p)
                <option value="{{ $p->user_id }}">{{ $p->user_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="float-right" style="width: 150px; margin-right: 10px;">
            <select class="form-control" name="order_status" id="order_status">
                <option value="">Select Order Status</option>
                <option value="Pending">Pending</option>
                <option value="In Progress">In Progress</option>
                <option value="Cancelled">Cancelled</option>
                <option value="Completed">Completed</option>
            </select>
        </div>
            <h3 class="card-title">
                <div class="mr-auto"> <i class="icon-cart"></i> List Orders</div>
            </h3>
            <div class="row">
                <div class="col-sm-12">
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}
                        </div>
                        @endif
                        @if(!$records->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 50px;">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" class="checkall">
                                            <span class="label-text"></span>
                                        </label>
                                    </th>
                                    <th style="width: 80px;">Sr. No.</th>
                                    <th>Order Number</th>
                                    <th>Mobile No.</th>
                                    <th>Email</th>
                                    <th>Service</th>
                                    <th>Order Amount</th>
                                    <th>Order Schedule Date &amp; Time</th>
                                    <th>Order Created At</th>
                                    <th>Partner Name</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach($records as $rec)
                                    <tr>
                                        <td>
                                            <label class="animated-checkbox">
                                                <input type="checkbox" name="check[]" value="{{ $rec->order_id  }}" class="check">
                                                <span class="label-text"></span>
                                            </label>
                                        </td>
                                        <td>{{ $sn++ }}.</td>
                                        <td>
                                            {{ $rec->order_id }}
                                        </td>
                                        <td>{{ $rec->user->user_mobile }}</td>
                                        <td>{{ $rec->user->user_email }}</td>
                                        <td>{{ @$rec->opro->service->service_name }}</td>
                                        <td>{{ $rec->order_total }}</td>
                                        <td>{{ $rec->order_schedule_date.' '.$rec->order_schedule_time }}</td>
                                        <td>{{ date("d-m-Y h:i A", strtotime($rec->order_created_on)) }}</td>
                                        <td class="text-center @if(empty($rec->partner->user_name)) text-danger @endif">{{ !empty($rec->partner->user_name) ? $rec->partner->user_name : 'Not Assigned' }}</td>
                                        <td class="text-center">{{ $rec->order_status }}</td>
                                        <td>
                                            <div style="margin-bottom: 10px;">
                                                <a href="{{ url('service-panel/invoice/'.$rec->order_id) }}" title="Invoice" data-toggle="tooltip" target="_blank"><i class="icon-print"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-4">
                <div class="card warning">
                    <h3 class="card-title">Instructions</h3>
                    <div>
                        <ul>
                            <li><i class="icon-check-square"></i> Admin can view users details</li>
                            <li><i class="icon-check-square"></i> Admin can go to add user page</li>
                            <li><i class="icon-check-square"></i> Admin can go to edit user's information</li>
                        </ul>
                    </div>
                </div>
            </div> -->
        </div>
    </form>
</div>
