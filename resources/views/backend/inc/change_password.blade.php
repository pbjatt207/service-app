<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Change Password</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href="{{ url('service-panel') }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Change Password</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <form method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-sm-4 m-auto">
                <div class="card">
                    <h3 class="card-title mb-5 text-center">
                        <i class="icon-lock_open"></i> Change Password
                    </h3>
                    @if (\Session::has('success'))
                    <div class="alert alert-success">
                        {!! \Session::get('success') !!}
                    </div>
                    @endif
                    @if (\Session::has('danger'))
                    <div class="alert alert-danger">
                        {!! \Session::get('danger') !!}
                    </div>
                    @endif
                    <div class="form-group">
                        <label>Current Password</label>
                        <input type="password" name="record[current_password]" placeholder="Current Password" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>New Password</label>
                        <input type="password" name="record[new_password]" placeholder="New Password" class="form-control password" required>
                    </div>

                    <div class="form-group">
                        <label>Re-type New Password</label>
                        <input type="password" name="record[renew_password]" placeholder="Re-type New Password" class="form-control confirm-password" required>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Change Password </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
