<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Categories</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Categories</li>
            </ul>
        </div>
    </div>
</section>
<div class="container-fluid">
    <div class="card">
        <form method="post">
            <h3 class="card-title">
                <i class="icon-sitemap"></i> {{ !empty($edit->category_id) ? "Edit" : "Add" }} Category
            </h3>
            @csrf
            <div class="row">
                <div class="col-8 col-lg-9">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="record[category_name]" value="{{ @$edit->category_name }}" placeholder="Name" class="form-control">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Parent</label>
                                <select class="form-control" name="record[category_parent]">
                                    <option value="0">ROOT</option>
                                    @if(!$mcategories->isEmpty())
                                        @foreach($mcategories as $c)
                                            <option value="{{ $c->category_id }}" @if(@$edit->category_parent == $c->category_id) selected @endif>{{ $c->category_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="category[category_description]" rows="8" class="form-control" placeholder="Category Description">{{ @$edit->category_description }}</textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">{{ !empty($edit->category_id) ? "Update" : "Save" }}</button>
                    </div>
                </div>
                <div class="col-4 col-lg-3">
                    
                </div>
            </div>
        </form>
    </div>

    <div class="card mt-5">
        <form method="post">
            <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a>
            <h3 class="card-title">
                <div class="mr-auto"><i class="icon-sitemap"></i> View Categories</div>
                <a href="" class="ml-auto text-white">
                    <i class="icon-trash-o"></i>
                </a>
            </h3>
            @csrf
            @if(!$records->isEmpty())
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 50px;">
                                <label class="animated-checkbox">
                                    <input type="checkbox" class="checkall">
                                    <span class="label-text"></span>
                                </label>
                            </th>
                            <th>S.No.</th>
                            <th>Image</th>
                            <th>Category Name</th>
                            <th>Parent</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @php $sn = $records->firstItem(); @endphp
                        @foreach($records as $rec)
                        <tr>
                            <td>
                                <label class="animated-checkbox">
                                    <input type="checkbox" name="check[]" value="{{ $rec->category_id  }}" class="check">
                                    <span class="label-text"></span>
                                </label>
                            </td>
                            <td>{{ $sn++ }}</td>
                            <td>{{ $rec->category_image }}</td>
                            <td>{{ $rec->category_name }}</td>
                            <td>{{ !empty($rec->mcategory) ? $rec->mcategory : "ROOT" }}</td>
                            <td class="icon-cent">
                                <a href="{{ url('service-panel/category/'.$rec->category_slug) }}" class="pencil"><i class="icon-pencil" title="Edit"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $records->links() }}
            @else
            <div class="no_records_found">
              No records found yet.
            </div>
            @endif
        </form>
    </div>
</div>
