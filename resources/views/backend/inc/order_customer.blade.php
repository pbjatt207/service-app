<section class="page-header mb-3">
    <div class="container-fluid subservice">
        <div class="clearfix">
            <div class="float-left">
                <h1>Order Customers</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href="{{ url('service-panel') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ url('servide-panel/order') }}">Order</a></li>
                <li class="breadcrumb-item active">Order Customers</li>
            </ul>
        </div>
    </div>
</section>
<div >
    <div class="card">
        <h3 class="card-title clearfix">
            <form class="form-inline">
                 <input type="text" name="search_keyword" value="{{ $s }}" class="form-control" placeholder="Search Customer">
                 <button type="submit" class="btn btn-primary"> <i class="icon-search"></i> GO </button>
            </form>
        </h3>
        <div class="basic-info-two">
            @if (\Session::has('success'))
            <div class="alert alert-success">
               {!! \Session::get('success') !!}
            </div>
            @endif
        </div>
    </div>
    <form method="post">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                @if(!$records->isEmpty())
                <div class="row">
                     @foreach($records as $rec)
                     <div class="col-sm-4 col-lg-3">
                         <div class="card mt-3">
                              <p>
                                   <i class="icon-person"></i> {{ $rec->user_name }}
                              </p>
                              <p style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">
                                   <i class="icon-envelope"></i> {{ $rec->user_email }}
                              </p>
                              <p>
                                   <i class="icon-mobile"></i> {{ $rec->user_mobile }}
                              </p>
                         </div>
                         <a href="{{ url('service-panel/order/add/'.$rec->user_id) }}" class="btn btn-primary btn-block">Select</a>
                     </div>
                     @endforeach
                </div>
                {{ $records->links() }}
                @else
                <div class="no_records_found">
                  No records found yet.
                </div>
                @endif
            </div>
            <!-- <div class="col-sm-4">
                <div class="card warning">
                    <h3 class="card-title">Instructions</h3>
                    <div>
                        <ul>
                            <li><i class="icon-check-square"></i> Admin can view users details</li>
                            <li><i class="icon-check-square"></i> Admin can go to add user page</li>
                            <li><i class="icon-check-square"></i> Admin can go to edit user's information</li>
                        </ul>
                    </div>
                </div>
            </div> -->
        </div>
    </form>
</div>
