<section class="page-header mb-3">
    <div class="container-fluid subservice">
        <div class="clearfix">
            <div class="float-left">
                <h1>{{ !empty($edit->coupon_id) ? "Edit" : "Add" }} Promo Coupon</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href=""> <i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ url('coupon') }}">Promo Coupon</a></li>
                <li class="breadcrumb-item active">{{ !empty($edit->coupon_id) ? "Edit" : "Add" }} Promo Coupon</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post" enctype="multipart/form-data" autocomplete="off">
            <input autocomplete="off" name="hidden" type="search" style="display:none;">
            @csrf
            @if (\Session::has('danger'))
                <div class="alert alert-danger">
                    {!! \Session::get('danger') !!}</li>
                </div>
            @endif
            <a href="#save-data" class="float-right"> <i class="icon-save"></i> {{ !empty($edit->coupon_id) ? "Update" : "Save" }} </a>
        	<h3 class="card-title">
        		<div class="mr-auto">General Information</div>
        	</h3>
        	<div class="row">
        		<div class="col-sm-8 col-lg-9">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Coupon Code (Required)</label>
                    			<input type="text" name="coupon[coupon_code]" value="{{ @$edit->coupon_code }}" placeholder="Coupon Code" class="form-control" required autocomplete="new_name">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Valid From (Required)</label>
                    			<input type="text" name="coupon[coupon_valid_from]" value="{{ @$edit->coupon_valid_from }}" placeholder="yyyy-mm-dd" class="form-control datepicker" autocomplete="new_name" readonly required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="float-right">
                                     <input type="checkbox" name="coupon[coupon_never_end]" value="1" id="neverEnd" @if(@strtotime($edit->coupon_valid_upto) <= 0) checked @endif;> Never End
                                </label>
                                <label>Valid Upto</label>
                                <input type="text" name="coupon[coupon_valid_upto]" value="{{ @strtotime($edit->coupon_valid_upto) > 0 ? $edit->coupon_valid_upto : "" }}" class="form-control datepicker" placeholder="yyyy-mm-dd" readonly id="validUpto" @if(@strtotime($edit->coupon_valid_upto) <= 0) disabled @endif;>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-sm-4">
                              <div class="form-group">
                                   <label>Coupon Type</label>
                                   <select class="form-control" name="coupon[coupon_type]">
                                        <option value="Discount"@if(@$edit->coupon_type == "Discount") selected @endif>Discount</option>
                                        <option value="Cashback"@if(@$edit->coupon_type == "Cashback") selected @endif>Cashback</option>
                                   </select>
                              </div>
                         </div>
                         <div class="col-sm-4">
                              <div class="form-group">
                                   <label>Coupon Discount Type</label>
                                   <select class="form-control" name="coupon[coupon_discount_type]">
                                        <option value="Percent"@if(@$edit->coupon_discount_type == "Percent") selected @endif>Percent</option>
                                        <option value="Flat"@if(@$edit->coupon_discount_type == "Flat") selected @endif>Flat</option>
                                   </select>
                              </div>
                         </div>
                         <div class="col-sm-4">
                              <div class="form-group">
                                   <label>Coupon Discount</label>
                                   <input type="text" name="coupon[coupon_discount]" class="form-control" placeholder="Coupon Discount" value="{{ @$edit->coupon_discount }}">
                              </div>
                         </div>
                    </div>
                    <div class="form-group">
                         <label>Coupon Short Description</label>
                         <textarea name="coupon[coupon_short_description]" rows="5" class="form-control" placeholder="Coupon Short Description">{{ @$edit->coupon_short_description }}</textarea>
                    </div>
                    <div class="form-group">
                         <label>Coupon Description</label>
                         <textarea name="coupon[coupon_description]" rows="8" class="form-control" placeholder="Coupon Description">{{ @$edit->coupon_description }}</textarea>
                    </div>
                    <div class="row check_enable">
                         <div class="col-sm-6">
                              <label>
                                   <input type="checkbox" name="coupon[coupon_is_user_limit]" value="1" @if(!empty($edit->coupon_limit_no)) checked @endif>
                                   Limit total no. of users
                              </label>
                         </div>
                         <div class="col-sm-6">
                              <div class="form-group">
                                   <input type="text" name="coupon[coupon_limit_no]" value="{{ @$edit->coupon_limit_no }}" class="form-control" @if(empty($edit->coupon_limit_no)) readonly @endif>
                              </div>
                         </div>
                    </div>
                    <div class="row check_enable">
                         <div class="col-sm-6">
                              <label>
                                   <input type="checkbox" name="coupon[coupon_is_total_uses]" value="1" @if(!empty($edit->coupon_is_total_uses)) checked @endif>
                                   Total no. of uses per person
                              </label>
                         </div>
                         <div class="col-sm-6">
                              <div class="form-group">
                                   <input type="text" name="coupon[coupon_total_uses]" value="{{ @$edit->coupon_total_uses }}" class="form-control" @if(empty($edit->coupon_total_uses)) readonly @endif>
                              </div>
                         </div>
                    </div>

                    <div class="row check_enable">
                         <div class="col-sm-6">
                              <label>
                                   <input type="checkbox" name="coupon[coupon_is_total_cart]" value="1" @if(!empty($edit->coupon_is_total_cart)) checked @endif>
                                   Need minimum cart total amount
                              </label>
                         </div>
                         <div class="col-sm-6">
                              <div class="form-group">
                                   <input type="text" name="coupon[coupon_min_cart_total]" value="{{ @$edit->coupon_min_cart_total }}" class="form-control" @if(empty($edit->coupon_min_cart_total)) readonly @endif>
                              </div>
                         </div>
                    </div>

                    <div class="row check_enable">
                         <div class="col-sm-6">
                              <label>
                                   <input type="checkbox" name="coupon[coupon_is_max_discount_amt]" value="1" @if(!empty($edit->coupon_is_max_discount_amt)) checked @endif>
                                   Need max discount amount
                              </label>
                         </div>
                         <div class="col-sm-6">
                              <div class="form-group">
                                   <input type="text" name="coupon[coupon_max_discount_amt]" value="{{ @$edit->coupon_max_discount_amt }}" class="form-control" @if(empty($edit->coupon_max_discount_amt)) readonly @endif>
                              </div>
                         </div>
                    </div>
        		</div>
                <div class="col-sm-4 col-lg-3">
                    <label class="upload_image">
                        <img src="{{ empty($edit->coupon_image) ? url('imgs/no-image.png') : url('imgs/coupons/'.$edit->coupon_image) }}" alt="Upload Image" title="Upload Image">
                        <input type="file" name="coupon_image" accept="image/*" id="userImage">
                    </label>
                    <label for="userImage" class="btn btn-primary btn-block">Choose File</label>

                    <div class="form-group">
                        <label>Select Service</label>
                        <select class="form-control service" data-target="#couponSubService" name="record[coupon_sid]">
                            <option value="">Select Service</option>
                            @foreach($services as $s)
                            <option value="{{ $s->service_id }}" @if(@$edit->coupon_sid == $s->service_id) selected @endif>{{ $s->service_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Select Subservice</label>
                        <select class="form-control" id="couponSubService" name="record[coupon_ssid]">
                            <option value="">Select Subservice</option>
                            @foreach($subservices as $s)
                            <option value="{{ $s->subservice_id }}" @if(@$edit->coupon_ssid == $s->subservice_id) selected @endif>{{ $s->subservice_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    @php
                    $uids = !empty($edit->coupon_uids) ? explode(",", $edit->coupon_uids) : [];
                    @endphp
                    <div class="form-group">
                        <label>Select User</label>
                        <select class="form-control select2" name="record[coupon_uids][]" multiple>
                            @foreach($users as $u)
                            <option value="{{ $u->user_id }}" @if(in_array($u->user_id, $uids)) selected @endif)>{{ $u->user_name.' '.$u->user_mobile }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
        	</div>
        </form>
    </div>
</div>
