<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Milestones</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Milestones</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post" enctype="multipart/form-data">
            @csrf
            <a href="#save-data" class="float-right"> <i class="icon-save"></i> {{ !empty($edit->ms_id) ? "Update" : "Save" }} </a>
            <h3 class="card-title">
                <i class="icon-pin_drop"></i> {{ !empty($edit->ms_id) ? "Edit" : "Add" }} Milestones
            </h3>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>No. Of Services</label>
                        <input type="number" name="record[ms_service_no]" value="{{ @$edit->ms_service_no }}" placeholder="No. of Services" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Reward Points</label>
                        <input type="number" name="record[ms_reward_point]" value="{{ @$edit->ms_reward_point }}" placeholder="Reward Points" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Remark</label>
                        <input type="text" name="record[ms_remark]" value="{{ @$edit->ms_remark }}" min="1" placeholder="Remark" class="form-control" required>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="card mt-5">
        <form method="post">
            <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a>
            <h3 class="card-title">
                <div class="mr-auto"><i class="icon-pin_drop"></i> View Milestones</div>
                <a href="" class="ml-auto text-white">
                    <i class="icon-trash-o"></i>
                </a>
            </h3>
            @csrf
            @if(!$records->isEmpty())
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 50px;">
                                <label class="animated-checkbox">
                                    <input type="checkbox" class="checkall">
                                    <span class="label-text"></span>
                                </label>
                            </th>
                            <th>S.No.</th>
                            <th>No. Of Services</th>
                            <th>Reward Points</th>
                            <th>Remark</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @php $sn = $records->firstItem(); @endphp
                        @foreach($records as $rec)
                        <tr>
                            <td>
                                <label class="animated-checkbox">
                                    <input type="checkbox" name="check[]" value="{{ $rec->ms_id  }}" class="check">
                                    <span class="label-text"></span>
                                </label>
                            </td>
                            <td width="50">{{ $sn++ }}</td>
                            <td>{{ $rec->ms_service_no }}</td>
                            <td>{{ $rec->ms_reward_point }}</td>
                            <td>{{ $rec->ms_remark }}</td>
                            <td class="icon-cent">
                                <a href="{{ url('service-panel/milestone/'.$rec->ms_id) }}" class="pencil"><i class="icon-pencil1" data-toggle="tooltip" title="Edit"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $records->links() }}
            @else
            <div class="no_records_found">
              No records found yet.
            </div>
            @endif
        </form>
    </div>
</div>
