<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Timeslots</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Timeslots</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post" enctype="multipart/form-data">
            @csrf
            <h3 class="card-title">
                <i class="icon-clock"></i> {{ !empty($edit->tslot_id) ? "Edit" : "Add" }} Timeslots
            </h3>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="record[tslot_name]" value="{{ @$edit->tslot_name }}" placeholder="Name" class="form-control" required>
                    </div>

                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min Time</label>
                        <input type="text" name="record[tslot_min_time]" value="{{ @$edit->tslot_min_time }}" placeholder="Min Time" class="form-control" required>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-primary btn-block">{{ !empty($edit->tslot_id) ? "Update" : "Save" }}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="card mt-5">
        <form method="post">
            <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a>
            <h3 class="card-title">
                <div class="mr-auto"><i class="icon-clock"></i> View Timeslots</div>
                <a href="" class="ml-auto text-white">
                    <i class="icon-trash-o"></i>
                </a>
            </h3>
            @csrf
            @if(!$records->isEmpty())
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 50px;">
                                <label class="animated-checkbox">
                                    <input type="checkbox" class="checkall">
                                    <span class="label-text"></span>
                                </label>
                            </th>
                            <th>S.No.</th>
                            <th>Name</th>
                            <th>Min Time</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @php $sn = $records->firstItem(); @endphp
                        @foreach($records as $rec)
                        <tr>
                            <td>
                                <label class="animated-checkbox">
                                    <input type="checkbox" name="check[]" value="{{ $rec->tslot_id  }}" class="check">
                                    <span class="label-text"></span>
                                </label>
                            </td>
                            <td width="50">{{ $sn++ }}</td>
                            <td>{{ $rec->tslot_name }}</td>
                            <td>{{ $rec->tslot_min_time }}</td>
                            <td class="icon-cent">
                                <a href="{{ url('service-panel/timeslot/'.$rec->tslot_id) }}" class="pencil"><i class="icon-pencil1" data-toggle="tooltip" title="Edit"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $records->links() }}
            @else
            <div class="no_records_found">
              No records found yet.
            </div>
            @endif
        </form>
    </div>
</div>
