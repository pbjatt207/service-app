<section class="page-header mb-3">
    <div class="container-fluid subservice">
        <div class="clearfix">
            <div class="float-left">
                <h1>{{ !empty($edit->choose_id) ? "Edit" : "Add" }} Why Choose Us</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href=""> <i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ url('choose') }}">Why Choose Us</a></li>
                <li class="breadcrumb-item active">{{ !empty($edit->choose_id) ? "Edit" : "Add" }} Why Choose Us</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post" enctype="multipart/form-data" autocomplete="off">
            <input autocomplete="off" name="hidden" type="search" style="display:none;">
            @csrf
            @if (\Session::has('danger'))
                <div class="alert alert-danger">
                    {!! \Session::get('danger') !!}</li>
                </div>
            @endif
            <a href="#save-data" class="float-right"> <i class="icon-save"></i> {{ !empty($edit->choose_id) ? "Update" : "Save" }} </a>
        	<h3 class="card-title">
        		<div class="mr-auto">General Information</div>
        	</h3>
        	<div class="row">
        		<div class="col-sm-8 col-lg-9">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Title (Required)</label>
                    		  <input type="text" name="record[choose_title]" value="{{ @$edit->choose_title }}" placeholder="Title" class="form-control" required autocomplete="new_name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                          <div class="col-sm-4 col-lg-3">
                              <label class="upload_image">
                                  <img src="{{ empty($edit->choose_icon) ? url('imgs/no-image.png') : url('imgs/choose/'.$edit->choose_icon) }}" alt="Upload Image" title="Upload Image">
                                  <input type="file" name="choose_icon" accept="image/*" id="chooseImage">
                              </label>
                              <label for="chooseImage" class="btn btn-primary btn-block">Choose File</label>
                          </div>
                    </div>
        		</div>
        	</div>
        </form>
    </div>
</div>
