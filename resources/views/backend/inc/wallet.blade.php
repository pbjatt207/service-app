<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Wallet</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Wallet</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post">
            @csrf
            <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a>
            <h3 class="card-title">
                <div class="mr-auto"><i class="icon-account_balance_wallet"></i> Wallet Summary</div>
                <a href="" class="ml-auto text-white">
                    <i class="icon-trash-o"></i>
                </a>
            </h3>
            <div class="text-center mb-3">
                <select class="form-control" name="search[user_id]" onchange="window.location = '{{ url('service-panel/wallet/?user_id=') }}'+this.value">
                    <option value="">Select User</option>
                    @foreach($users as $u)
                    <option value="{{ $u->user_id }}" @if($wallet_uid == $u->user_id) selected @endif>{{ $u->user_name.' '.$u->user_mobile.' ('.$u->user_role.')' }}</option>
                    @endforeach
                </select>
            </div>
            @if(!empty($records))
                @if(!$records->isEmpty())
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 50px;">
                                    <label class="animated-checkbox">
                                        <input type="checkbox" class="checkall">
                                        <span class="label-text"></span>
                                    </label>
                                </th>
                                <th>S.No.</th>
                                <th>Name</th>
                                <th>Mobile No.</th>
                                <th>Role</th>
                                <th>Remarks</th>
                                <th>Amount</th>
                                <th>Type</th>
                                <th>Balance</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $sn = $records->firstItem(); $balance = 0; @endphp
                            @foreach($records as $rec)
                                @php
                                    if($rec->wallet_type == "Credit") {
                                        $balance += $rec->wallet_amount;
                                    } else {
                                        $balance -= $rec->wallet_amount;
                                    }
                                @endphp
                            <tr class="{{ $rec->wallet_type == 'Credit' ? 'bg-success' : 'bg-danger' }} text-white">
                                <td>
                                    <label class="animated-checkbox">
                                        <input type="checkbox" name="check[]" value="{{ $rec->wallet_id  }}" class="check">
                                        <span class="label-text"></span>
                                    </label>
                                </td>
                                <td width="50">{{ $sn++ }}</td>
                                <td>{{ $rec->user->user_name }}</td>
                                <td>{{ $rec->user->user_mobile }}</td>
                                <td>{{ $rec->user->user_role }}</td>
                                <td>{{ $rec->wallet_remarks }}</td>
                                <td>{{ $rec->wallet_amount }}</td>
                                <td>{{ $rec->wallet_type }}</td>
                                <td class="icon-cent">
                                    {{ $balance }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $records->links() }}
                @else
                <div class="no_records_found alert alert-warning">
                  No records found yet.
                </div>
                @endif
            @endif
        </form>
    </div>
</div>
