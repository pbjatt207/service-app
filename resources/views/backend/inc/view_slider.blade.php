<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>View Sliders</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href="">Dashboard</a></li>
                <li class="breadcrumb-item active">Sliders</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <form method="post">
        @csrf
            <div class="card">
        <div class="row">
            <div class="col-sm-12">
                    <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a>
                    <h3 class="card-title clearfix">
                        <div class="mr-auto"> <i class="icon-images"></i> List Sliders</div>
                    </h3>
                    <div class="basic-info-two">
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}
                        </div>
                        @endif
                        @if(!$records->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 50px;">
                                        <label class="animated-checkbox">
                                            <input type="checkbox" class="checkall">
                                            <span class="label-text"></span>
                                        </label>
                                    </th>
                                    <th style="width: 80px;">Sr. No.</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Service</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sn = $records->firstItem(); @endphp
                                @foreach($records as $rec)
                                    @php
                                    $image  = !empty($rec->slider_image) ? "imgs/sliders/".$rec->slider_image : "imgs/no-image.png";
                                    @endphp
                                    <tr>
                                        <td>
                                            <label class="animated-checkbox">
                                                <input type="checkbox" name="check[]" value="{{ $rec->slider_id  }}" class="check">
                                                <span class="label-text"></span>
                                            </label>
                                        </td>
                                        <td>{{ $sn++ }}.</td>
                                        <td>
                                            {{ $rec->slider_name }}
                                        </td>
                                        <td width="300"><img src="{{ url($image) }}" alt="{{ $rec->service_name }}" title="{{ $rec->service_name }}" style="width: 256px;"></td>
                                        <td>
                                            {{ $rec->service_name }}
                                        </td>
                                        <td>
                                            <div style="margin-bottom: 10px;">
                                                <a href="{{ url('service-panel/slider/add/'.$rec->slider_id) }}" title="Edit" data-toggle="tooltip"><i class="icon-pencil1"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                        {{ $records->links() }}
                        @else
                        <div class="no_records_found">
                          No records found yet.
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-4">
                <div class="card warning">
                    <h3 class="card-title">Instructions</h3>
                    <div>
                        <ul>
                            <li><i class="icon-check-square"></i> Admin can view users details</li>
                            <li><i class="icon-check-square"></i> Admin can go to add user page</li>
                            <li><i class="icon-check-square"></i> Admin can go to edit user's information</li>
                        </ul>
                    </div>
                </div>
            </div> -->
        </div>
    </form>
</div>
