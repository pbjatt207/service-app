<section class="page-header mb-3">
    <div class="container-fluid subservice">
        <div class="clearfix">
            <div class="float-left">
                <h1>Type Of Services</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Type Of Services</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <div class="card mt-5">
        <form method="post">
            <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a>
            <h3 class="card-title">
                <div class="mr-auto"><i class="icon-wrench"></i> View Type Of Services</div>
            </h3>
            @csrf
            @if(!$records->isEmpty())
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 50px;">
                                <label class="animated-checkbox">
                                    <input type="checkbox" class="checkall">
                                    <span class="label-text"></span>
                                </label>
                            </th>
                            <th>S.No.</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Tagline</th>
                            <th>Service</th>
                            <th>Wishes</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @php $sn = $records->firstItem(); @endphp
                        @foreach($records as $rec)
                            @php
                            $image  = !empty($rec->subservice_image) ? "imgs/services/".$rec->subservice_image : "imgs/no-image.png";
                            $wishes = !empty($rec->subservice_wish)  ? unserialize($rec->subservice_wish) : [];
                            $wishes = implode("<br>", $wishes);
                            @endphp
                        <tr>
                            <td>
                                <label class="animated-checkbox">
                                    <input type="checkbox" name="check[]" value="{{ $rec->subservice_id  }}" class="check">
                                    <span class="label-text"></span>
                                </label>
                            </td>
                            <td>{{ $sn++ }}</td>
                            <td width="100"> <img src="{{ url($image) }}" alt="{{ $rec->subservice_name }}" title="{{ $rec->subservice_name }}" style="width: 92px;"> </td>
                            <td>{{ $rec->subservice_name }}</td>
                            <td>{{ $rec->subservice_tagline }}</td>
                            <td>{{ $rec->service_name }}</td>
                            <td>{!! $wishes !!}</td>
                            <td class="icon-cent">
                                <a href="{{ url('service-panel/service/add_subservice/'.$rec->subservice_slug) }}" class="pencil"><i class="icon-pencil" title="Edit"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $records->links() }}
            @else
            <div class="no_records_found">
              No records found yet.
            </div>
            @endif
        </form>
    </div>
</div>
