<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>{{ !empty($edit->page_id) ? "Edit" : "Add" }} Page</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href=""> <i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ url('coupon') }}">Sliders</a></li>
                <li class="breadcrumb-item active">{{ !empty($edit->page_id) ? "Edit" : "Add" }} Page</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post" enctype="multipart/form-data" autocomplete="off">
            <input autocomplete="off" name="hidden" type="search" style="display:none;">
            @csrf
            @if (\Session::has('danger'))
                <div class="alert alert-danger">
                    {!! \Session::get('danger') !!}</li>
                </div>
            @endif
            <a href="#save-data" class="float-right"> <i class="icon-save"></i> {{ !empty($edit->page_id) ? "Update" : "Save" }} </a>
        	<h3 class="card-title">
        		<div class="mr-auto">General Information</div>
        	</h3>

            <div class="form-group">
                <label>Page Title (Required)</label>
		        <input type="text" name="record[page_title]" value="{{ @$edit->page_title }}" placeholder="Page Title" class="form-control" required autocomplete="new_name">
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea name="record[page_description]" rows="8" class="form-control editor">{{ @$edit->page_description }}</textarea>
            </div>
        </form>
    </div>
</div>
