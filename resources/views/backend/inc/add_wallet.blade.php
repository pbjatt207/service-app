<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Add Wallet</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Add Wallet</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post" enctype="multipart/form-data">
            @csrf
            <a href="#save-data" class="float-right"> <i class="icon-save"></i> {{ !empty($edit->ms_id) ? "Update" : "Save" }} </a>
            <h3 class="card-title">
                <i class="icon-account_balance_wallet"></i> {{ !empty($edit->ms_id) ? "Edit" : "Add" }} Wallet
            </h3>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Select User</label>
                        <select class="form-control" name="record[wallet_uid]" required>
                            <option value="">Select User</option>
                            @foreach($users as $u)
                            <option value="{{ $u->user_id }}" @if(@$wallet_uid == $u->user_id) selected @endif>{{ $u->user_name.' '.$u->user_mobile.' ('.$u->user_role.')' }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Wallet Type</label>
                        <select class="form-control" name="record[wallet_type]">
                            <option value="Credit">Credit</option>
                            <option value="Debit">Debit</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="number" name="record[wallet_amount]" value="{{ @$edit->wallet_amount }}" min="1" placeholder="Amount" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Remark</label>
                        <input type="text" name="record[wallet_remarks]" value="{{ @$edit->wallet_remarks }}" placeholder="Remark" class="form-control" required>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
