<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Services</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Services</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post" enctype="multipart/form-data">
            @csrf
            <a href="#save-data" class="float-right"> <i class="icon-save"></i> {{ !empty($edit->service_id) ? "Update" : "Save" }} </a>
            <h3 class="card-title">
                <i class="icon-briefcase"></i> {{ !empty($edit->service_id) ? "Edit" : "Add" }} Service
            </h3>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="record[service_name]" value="{{ @$edit->service_name }}" placeholder="Name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Tagline (optional)</label>
                        <input type="text" name="record[service_tagline]" value="{{ @$edit->service_tagline }}" placeholder="Tagline (Optional)" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Cost <small>(Per Hour)</small></label>
                        <input type="number" name="record[service_cost]" value="{{ @$edit->service_cost }}" min="1" placeholder="Cost (Per Hour)" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Order Limit <small>(Per Time slot)</small></label>
                        <input type="number" name="record[service_order_limit]" value="{{ @$edit->service_order_limit }}" min="1" placeholder="Orer Limit (Per Time slot)" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Wishes</label>
                        <div id="wishRow">
                            @if(empty($edit->service_wish))
                            <div class="row form-group">
                                <div class="col-sm-10 col-9">
                                    <input type="text" name="record[service_wish][]" value="" class="form-control" placeholder="Wishes">
                                </div>
                                <div class="col-sm-2 col-3 text-center">
                                    <button type="button" class="btn btn-block btn-success add_wish_row"> <i class="icon-plus-circle"></i> </button>
                                </div>
                            </div>
                            @else
                                @php $wishes = unserialize($edit->service_wish); $i = 0;  @endphp
                                @foreach($wishes as $w)
                                    @php
                                        $i++;
                                    @endphp
                                    <div class="row form-group">
                                        <div class="col-sm-10 col-9">
                                            <input type="text" name="record[service_wish][]" value="{{ $w }}" class="form-control" placeholder="Wishes">
                                        </div>
                                        <div class="col-sm-2 col-3 text-center">
                                            <button type="button" class="btn btn-block {{ $i == 1 ? 'btn-success add_wish_row' : 'btn-danger remove_wish_row' }}"> <i class="{{ $i == 1 ? 'icon-plus-circle' : 'icon-minus-circle' }}"></i> </button>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <label class="upload_image">
                        <img src="{{ empty($edit->service_icon) ? url('imgs/no-image.png') : url('imgs/services/'.$edit->service_icon) }}" alt="Upload Image" title="Upload Image">
                        <input type="file" name="service_icon" accept="image/*" id="serviceIcon">
                    </label>
                    <label for="serviceIcon" class="btn btn-primary btn-block">Choose File</label>
                </div>
                <div class="col-sm-3">
                    <label class="upload_image">
                        <img src="{{ empty($edit->service_image) ? url('imgs/no-image.png') : url('imgs/services/'.$edit->service_image) }}" alt="Upload Image" title="Upload Image">
                        <input type="file" name="service_image" accept="image/*" id="serviceImage">
                    </label>
                    <label for="serviceImage" class="btn btn-primary btn-block">Choose File</label>
                </div>
            </div>
        </form>
    </div>

    <div class="card mt-5">
        <form method="post">
            <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a>
            <h3 class="card-title">
                <div class="mr-auto"><i class="icon-briefcase"></i> View Services</div>
                <a href="" class="ml-auto text-white">
                    <i class="icon-trash-o"></i>
                </a>
            </h3>
            @csrf
            @if(!$records->isEmpty())
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 50px;">
                                <label class="animated-checkbox">
                                    <input type="checkbox" class="checkall">
                                    <span class="label-text"></span>
                                </label>
                            </th>
                            <th>S.No.</th>
                            <th>Image</th>
                            <th>Icon</th>
                            <th>Service Name</th>
                            <th>Cost <small>(per hour)</small></th>
                            <th>Wishes</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @php $sn = $records->firstItem(); @endphp
                        @foreach($records as $rec)
                            @php
                            $image  = !empty($rec->service_image) ? "imgs/services/".$rec->service_image : "imgs/no-image.png";
                            $icon   = !empty($rec->service_icon) ? "imgs/services/".$rec->service_icon : "imgs/no-image.png";

                            $wishes = @unserialize($rec->service_wish);
                            $wishes = !empty($wishes) ? $wishes : [];
                            @endphp
                        <tr>
                            <td>
                                <label class="animated-checkbox">
                                    <input type="checkbox" name="check[]" value="{{ $rec->service_id  }}" class="check">
                                    <span class="label-text"></span>
                                </label>
                            </td>
                            <td width="50">{{ $sn++ }}</td>
                            <td width="100"><img src="{{ url($image) }}" alt="{{ $rec->service_name }}" title="{{ $rec->service_name }}" style="width: 92px;"></td>
                            <td width="100"><img src="{{ url($icon) }}" alt="{{ $rec->service_name }}" title="{{ $rec->service_name }}" style="width: 92px;"></td>
                            <td>{{ $rec->service_name }}</td>
                            <td>₹ {{ $rec->service_cost }}</td>
                            <td>
                                @foreach($wishes as $wish)
                                    <div>{!! $wish !!}</div>
                                @endforeach
                            </td>
                            <td class="icon-cent">
                                <a href="{{ url('service-panel/service/'.$rec->service_slug) }}" class="pencil"><i class="icon-pencil1" data-toggle="tooltip" title="Edit"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $records->links() }}
            @else
            <div class="no_records_found">
              No records found yet.
            </div>
            @endif
        </form>
    </div>
</div>
