<section class="page-header mb-3">
    <div class="container-fluid subservice">
        <div class="clearfix">
            <div class="float-left">
                <h1>{{ !empty($edit->user_id) ? "Edit" : "Add" }} Partner</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href=""> <i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ url('service-panel/partner') }}">Partner</a></li>
                <li class="breadcrumb-item active">{{ !empty($edit->user_id) ? "Edit" : "Add" }} Partner</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post" enctype="multipart/form-data" autocomplete="off">
            <input autocomplete="off" name="hidden" type="search" style="display:none;">
            @csrf
            @if (\Session::has('danger'))
                <div class="alert alert-danger">
                    {!! \Session::get('danger') !!}</li>
                </div>
            @endif
            <a href="#save-data" class="float-right"> <i class="icon-save"></i> {{ !empty($edit->user_id) ? "Update" : "Save" }} </a>
        	<h3 class="card-title">
        		<div class="mr-auto">Information</div>
        	</h3>
        	<div class="row">
        		<div class="col-sm-8 col-lg-9">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>First Name (Required)</label>
                    			<input type="text" name="user[user_fname]" value="{{ @$edit->user_fname }}" placeholder="First Name" class="form-control" required autocomplete="new_name">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Last Name</label>
                    			<input type="text" name="user[user_lname]" value="{{ @$edit->user_lname }}" placeholder="Last Name" class="form-control" autocomplete="new_name">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Date Of Birth</label>
                                <input type="text" name="user[user_dob]" value="{{ @$edit->user_dob }}" class="form-control datepicker_dob" placeholder="yyyy-mm-dd" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Mobile No. (Required)</label>
                    			<input type="tel" name="user[user_mobile]" value="{{ @$edit->user_mobile }}" placeholder="Mobile No." class="form-control" required autocomplete="new_mobile">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Email ID (Required)</label>
                    			<input type="email" name="user[user_email]" value="{{ @$edit->user_email }}" placeholder="Email ID" class="form-control" autocomplete="new_email" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Gender (Required)</label>
                    			<select class="form-control" name="user[user_gender]" required>
                                    <option value="">Select Gender</option>
                                    <option value="Male" @if(@$edit->user_gender == "Male") selected @endif>Male</option>
                                    <option value="Female" @if(@$edit->user_gender == "Female") selected @endif>Female</option>
                                    <option value="Other" @if(@$edit->user_gender == "Other") selected @endif>Other</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Experience (Required)</label>
                    			<input type="text" name="partner[partner_experience]" value="{{ @$edit->partner->partner_experience }}" placeholder="Experience" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Marital Status (Required)</label>
                    			<select class="form-control" name="partner[partner_marital_status]" required>
                                    <option value="">Select Marital Status</option>
                                    <option value="Single" @if(@$edit->partner->partner_marital_status == "Single") selected @endif>Single</option>
                                    <option value="Married" @if(@$edit->partner->partner_marital_status == "Married") selected @endif>Married</option>
                                    <option value="In Relationship" @if(@$edit->partner->partner_marital_status == "In Relationship") selected @endif>In Relationship</option>
                                    <option value="Widowed" @if(@$edit->partner->partner_marital_status == "Widowed") selected @endif>Widowed</option>
                                    <option value="Devorced" @if(@$edit->partner->partner_marital_status == "Devorced") selected @endif>Devorced</option>
                                    <option value="I don't know" @if(@$edit->partner->partner_marital_status == "I don't know") selected @endif>I don't know</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Parent / Spouce Name (Required)</label>
                    			<input type="text" name="partner[partner_spouse_name]" value="{{ @$edit->partner->partner_spouse_name }}" class="form-control" placeholder="Parent / Spouce Name" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Address 1 (Required)</label>
                        <input type="text" name="user[user_address1]" value="{{ @$edit->user_address1 }}" class="form-control" placeholder="Address 1" required>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Address 2</label>
                                <input type="text" name="user[user_address2]" value="{{ @$edit->user_address1 }}" class="form-control" placeholder="Address 2">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Pincode (Required)</label>
                                <input type="text" name="user[user_pincode]" value="{{ @$edit->user_pincode }}" class="form-control" placeholder="Pincode" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Country (Required)</label>
                    			<select class="form-control" name="user[user_country]" required>
                                    <option value="">Select Country</option>
                                    @foreach($countries as $c)
                                    <option value="{{ $c->country_id }}" @if(@$edit->user_country == $c->country_id) selected @endif>{{ $c->country_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>State (Required)</label>
                    			<select class="form-control" name="user[user_state]" required>
                                    <option value="">Select State</option>
                                    @if(!empty($states))
                                        @foreach($states as $s)
                                        <option value="{{ $s->state_id }}" @if(@$edit->user_state == $s->state_id) selected @endif>{{ $s->state_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>City (Required)</label>
                    			<select class="form-control" name="user[user_city]" required>
                                    <option value="">Select City</option>
                                    @if(!empty($cities))
                                        @foreach($cities as $c)
                                        <option value="{{ $c->city_id }}" @if(@$edit->user_city == $c->city_id) selected @endif>{{ $c->city_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Aadhaar No. (Required)</label>
                                <input type="text" name="partner[partner_uid_no]" value="{{ @$edit->partner->partner_uid_no }}" placeholder="Aadhaar No." class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>PAN No. (Required)</label>
                                <input type="text" name="partner[partner_pan_no]" value="{{ @$edit->partner->partner_pan_no }}" placeholder="PAN No." class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>About Him</label>
                        <textarea name="partner[partner_about]" rows="8" class="form-control">{{ @$edit->partner->partner_about }}</textarea>
                    </div>
        		</div>
                <div class="col-sm-4 col-lg-3">
                    <div class="form-group">
                        <label>Upload Photograph</label>
                        <label class="upload_image">
                            <img src="{{ empty($edit->user_image) ? url('imgs/no-image.png') : url('imgs/users/'.$edit->user_image) }}" alt="Upload Image" title="Upload Image">
                            <input type="file" name="user_image" accept="image/*" id="userImage">
                        </label>
                        <label for="userImage" class="btn btn-primary btn-block">Choose File</label>
                    </div>

                    <div class="form-group">
                        <label>Upload Aadhaar</label>
                        <input type="file" name="partner_uid_file" class="form-control">
                    </div>

                    @if(!empty($edit->partner->partner_uid_file))
                    <div class="form-group">
                        <a href="{{ url('uploads/users/'.$edit->partner->partner_uid_file) }}" target="_blank">View / Download</a>
                    </div>
                    @endif

                    <div class="form-group">
                        <label>Upload PAN</label>
                        <input type="file" name="partner_pan_file" class="form-control">
                    </div>

                    @if(!empty($edit->partner->partner_pan_file))
                    <div class="form-group">
                        <a href="{{ url('uploads/users/'.$edit->partner->partner_pan_file) }}" target="_blank">View / Download</a>
                    </div>
                    @endif

                    <div class="form-group">
                        <label>Upload Police Verification</label>
                        <input type="file" name="partner_police_verification" class="form-control">
                    </div>

                    @if(!empty($edit->partner->partner_police_verification))
                    <div class="form-group">
                        <a href="{{ url('uploads/users/'.$edit->partner->partner_police_verification) }}" target="_blank">View / Download</a>
                    </div>
                    @endif

                    <div class="form-group">
                        <label>Upload ID-Card</label>
                        <input type="file" name="partner_id_file" class="form-control">
                    </div>

                    @if(!empty($edit->partner->partner_id_file))
                    <div class="form-group">
                        <a href="{{ url('uploads/users/'.$edit->partner->partner_id_file) }}" target="_blank">View / Download</a>
                    </div>
                    @endif
                </div>
        	</div>
        </form>
    </div>
</div>
