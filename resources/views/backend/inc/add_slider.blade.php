<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>{{ !empty($edit->slider_id) ? "Edit" : "Add" }} Sliders</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href=""> <i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ url('coupon') }}">Sliders</a></li>
                <li class="breadcrumb-item active">{{ !empty($edit->slider_id) ? "Edit" : "Add" }} Sliders</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post" enctype="multipart/form-data" autocomplete="off">
            <input autocomplete="off" name="hidden" type="search" style="display:none;">
            @csrf
            @if (\Session::has('danger'))
                <div class="alert alert-danger">
                    {!! \Session::get('danger') !!}</li>
                </div>
            @endif
            <a href="#save-data" class="float-right"> <i class="icon-save"></i> {{ !empty($edit->slider_id) ? "Update" : "Save" }} </a>
        	<h3 class="card-title">
        		<div class="mr-auto">General Information</div>
        	</h3>
        	<div class="row">
        		<div class="col-sm-8 col-lg-9">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Slider Name (Required)</label>
                    		  <input type="text" name="record[slider_name]" value="{{ @$edit->slider_name }}" placeholder="Slider Name" class="form-control" required autocomplete="new_name">
                            </div>
                            <div class="form-group">
                                <label>Select Service (Required)</label>
                    		    <select class="form-control" name="record[slider_sid]" required>
                                    <option value="">Select Service</option>
                                    @foreach($services as $s)
                                    <option value="{{ $s->service_id }}" @if(@$edit->slider_sid == $s->service_id) selected @endif>{{ $s->service_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                          <div class="col-sm-4 col-lg-3">
                              <label class="upload_image">
                                  <img src="{{ empty($edit->slider_image) ? url('imgs/no-image.png') : url('imgs/sliders/'.$edit->slider_image) }}" alt="Upload Image" title="Upload Image">
                                  <input type="file" name="slider_image" accept="image/*" id="sliderImage">
                              </label>
                              <label for="sliderImage" class="btn btn-primary btn-block">Choose File</label>
                          </div>
                    </div>
        		</div>
        	</div>
        </form>
    </div>
</div>
