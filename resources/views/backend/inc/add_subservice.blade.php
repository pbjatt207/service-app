<section class="page-header mb-3 subservice">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Type Of Service</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href="{{ url('service-panel') }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ url('service-panel/service/subservices') }}">Type Of Service</a></li>
                <li class="breadcrumb-item active" aria-current="page">Add Type Of Service</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post" enctype="multipart/form-data">
            <a href="#save-data" class="float-right"> <i class="icon-save"></i> {{ !empty($edit->subservice_id) ? "Update" : "Save" }} </a>
            <h3 class="card-title">
                <i class="icon-wrench"></i> {{ !empty($edit->service_id) ? "Edit" : "Add" }} Type Of Service
            </h3>
            @csrf
            <div class="row">
                <div class="col-sm-8 col-lg-9">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="record[subservice_name]" value="{{ @$edit->subservice_name }}" placeholder="Name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Tagline</label>
                        <input type="text" name="record[subservice_tagline]" value="{{ @$edit->subservice_tagline }}" placeholder="Tagline" class="form-control">
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Service</label>
                                <select class="form-control" name="record[subservice_sid]">
                                    <option value="">Choose an service</option>
                                    @foreach($services as $s)
                                    <option value="{{ $s->service_id }}" @if(@$edit->subservice_sid == $s->service_id) selected @endif>{{ $s->service_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Cost (per hour)</label>
                                <input type="number" name="record[subservice_price]" value="{{ @$edit->subservice_price }}" placeholder="Cost (per hour)" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Wishes</label>
                        <div id="wishRow">
                            @if(empty($edit->subservice_wish))
                            <div class="row form-group">
                                <div class="col-sm-11 col-9">
                                    <input type="text" name="record[subservice_wish][]" value="" class="form-control" placeholder="Wishes">
                                </div>
                                <div class="col-sm-1 col-3 text-center">
                                    <button type="button" class="btn btn-block btn-success add_wish_row"> <i class="icon-plus-circle"></i> </button>
                                </div>
                            </div>
                            @else
                                @php $wishes = unserialize($edit->subservice_wish); $i = 0;  @endphp
                                @foreach($wishes as $w)
                                    @php
                                        $i++;
                                    @endphp
                                    <div class="row form-group">
                                        <div class="col-sm-11 col-9">
                                            <input type="text" name="record[subservice_wish][]" value="{{ $w }}" class="form-control" placeholder="Wishes">
                                        </div>
                                        <div class="col-sm-1 col-3 text-center">
                                            <button type="button" class="btn btn-block {{ $i == 1 ? 'btn-success add_wish_row' : 'btn-danger remove_wish_row' }}"> <i class="{{ $i == 1 ? 'icon-plus-circle' : 'icon-minus-circle' }}"></i> </button>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-3">
                    <label class="upload_image">
                        <img src="{{ empty($edit->subservice_image) ? url('imgs/no-image.png') : url('imgs/services/'.$edit->subservice_image) }}" alt="Upload Image" title="Upload Image">
                        <input type="file" name="subservice_image" accept="image/*" id="subserviceImage">
                    </label>
                    <label for="subserviceImage" class="btn btn-primary btn-block">Choose File</label>
                </div>
            </div>
            <div class="form-group">
                <label></label>
            </div>
        </form>
    </div>
</div>
