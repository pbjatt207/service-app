<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <base href="{{ url('/').'/' }}">
  <title>{{ $title }}</title>
  {{ HTML::style('css/bootstrap.min.css') }}
  {{ HTML::style('icomoon/style.css') }}
  <!-- {{ HTML::style('css/bootstrap-tagsinput.css') }} -->
  {{ HTML::style('css/jquery-ui.css') }}
  {{ HTML::style('admin/css/select2.min.css') }}
  {{ HTML::style('admin/css/style.css') }}
  {{ HTML::style('admin/css/theme/green.css') }}
</head>
<body class="dash_body">
	<input type="hidden" id="base_url" value="{{ url('service-panel') }}">
	<div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-lg-2 left_sidebar">
                <div class="sidebar_header text-center">
                    <div class="profile-img">
                        <img src="http://rudrakshatech.com/public/imgs/logo.png" alt="">
                    </div>
                    <h4 class="">Service App</h4>
                </div>
                <ul>
                    <li class="active">
                        <a href="{{ url('service-panel') }}">
                            <i class="icon-dashboard"></i>
                            Dashboard
                        </a>
                    </li>
                    @if($profile->user_role == "admin")
                    <li class="dropdown">
                        <a href="#">
                            <i class="icon-globe"></i>
                            Location
                            <span class="icon-angle-right float-right"></span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('service-panel/location/countries') }}">Countries</a>
                                <a href="{{ url('service-panel/location/states') }}">States</a>
                                <a href="{{ url('service-panel/location/cities') }}">Cities</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#">
                            <i class="icon-images"></i>
                            Slider
                            <span class="icon-angle-right float-right"></span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('service-panel/slider/add') }}">Add Slider</a>
                                <a href="{{ url('service-panel/slider') }}">View Slider</a>
                            </li>
                        </ul>
                    </li>
                    @php
                    $pages = DB::table('pages')->get();
                    @endphp
                    <li class="dropdown">
                        <a href="#">
                            <i class="icon-note"></i>
                            Pages
                            <span class="icon-angle-right float-right"></span>
                        </a>
                        <ul>
                            <li>
                                @foreach($pages as $p)
                                <a href="{{ url('service-panel/page/edit/'.$p->page_slug) }}">{{ $p->page_title }}</a>
                                @endforeach
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ url('service-panel/service') }}">
                            <i class="icon-briefcase"></i>
                            Services
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#">
                            <i class="icon-wrench"></i>
                            Type Of Services
                            <span class="icon-angle-right float-right"></span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('service-panel/service/add_subservice') }}">Add Type Of Services</a>
                                <a href="{{ url('service-panel/service/subservices') }}">View Type Of Services</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#">
                            <i class="icon-help"></i>
                            Why Choose Us
                            <span class="icon-angle-right float-right"></span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('service-panel/choose/add') }}">Add Why Choose Us</a>
                                <a href="{{ url('service-panel/choose/') }}">View Why Choose Us</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ url('service-panel/timeslot/') }}">
                            <i class="icon-clock"></i>
                            Timeslot
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#">
                            <i class="icon-account_balance_wallet"></i>
                            Wallets
                            <span class="icon-angle-right float-right"></span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('service-panel/wallet/add') }}">Add Wallet</a>
                                <a href="{{ url('service-panel/wallet') }}">Wallet Summary</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#">
                            <i class="icon-person"></i>
                            Customer
                            <span class="icon-angle-right float-right"></span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('service-panel/user/add/customer') }}">Add Customer</a>
                                <a href="{{ url('service-panel/user/customer') }}">View Customer</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#">
                            <i class="icon-credit-card2"></i>
                            Promo Coupons
                            <span class="icon-angle-right float-right"></span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('service-panel/coupon/add') }}">Add Promo Coupons</a>
                                <a href="{{ url('service-panel/coupon') }}">View Promo Coupons</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#">
                            <i class="icon-cart"></i>
                            Orders
                            <span class="icon-angle-right float-right"></span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('service-panel/order') }}">View All Orders</a>
                                <a href="{{ url('service-panel/order/?status=Upcoming') }}">Upcoming Orders</a>
                                <a href="{{ url('service-panel/order/?status=Pending') }}">Pending Orders</a>
                                <a href="{{ url('service-panel/order/?status=In Profress') }}">In Profress Orders</a>
                                <a href="{{ url('service-panel/order/?status=Completed') }}">Completed Orders</a>
                            </li>
                        </ul>
                    </li>
                    @elseif($profile->user_role == "partner-admin")
                    <li>
                        <a href="{{ url('service-panel/milestone') }}">
                            <i class="icon-pin_drop"></i>
                            Milestone
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#">
                            <i class="icon-person"></i>
                            Partners
                            <span class="icon-angle-right float-right"></span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('service-panel/user/add-partner') }}">Add Partner</a>
                                <a href="{{ url('service-panel/partner') }}">View Partner</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#">
                            <i class="icon-account_balance_wallet"></i>
                            Wallets
                            <span class="icon-angle-right float-right"></span>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('service-panel/wallet/add') }}">Add Wallet</a>
                                <a href="{{ url('service-panel/wallet') }}">Wallet Summary</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
            <div class="col-md-9 col-lg-10 offset-lg-2 offset-md-3">
                <div class="content_header">
                    <div class="row">
                        <div class="col-md-8"> </div>
                        <div class="col-md-4">
                            <ul>
                                <li> <a href="#"> <i class="icon-bell2"></i> </a> </li>
                                <li class="dropdown">
                                    <a href="#" class="profile_img" data-toggle="dropdown"> <img src="{{ url('admin/images/Users-Administrator-icon.png') }}" alt=""> Hello! {{ $profile->user_name }} <i class="icon-angle-down" style="font-size: inherit;"></i> </a>
                                    <ul class="dropdown-menu text-left">
                                        <li> <a href="{{ 'service-panel/settings' }}"> <i class="icon-cog"></i> Settings</a> </li>
                                        <li> <a href="{{ url('service-panel/change-password') }}"> <i class="icon-lock_open"></i> Change Password</a> </li>
                                        <li> <a href="{{ url('service-panel/user/logout') }}"> <i class="icon-power-off"></i> Logout</a> </li>
                                    </ul>
                                </li>
                                <li style="margin-right: -15px;" class="d-inline-block d-md-none d-lg-none">
                                    <a href="#nav-open"> <i class="icon-bars"></i> </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
