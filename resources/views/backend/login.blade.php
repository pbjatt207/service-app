<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Service App</title>

    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('icomoon/style.css') }}
    {{ HTML::style('admin/css/style.css') }}
    {{ HTML::style('admin/css/theme/green.css') }}

    {{--    <link rel="icon" href="{{ url('imgs/ea_logo.ico') }}"> --}}
</head>
<body>
<section class="login-page">
	<input type="hidden" id="base_url" value="{{ url('service-panel') }}">
    <div class="half"></div>
    <div class="container">
        <form id="loginForm" method="post" action="{{ url('service-panel/ajax/user_login') }}">
        @csrf

        <div class="sec">
            <h3 class="text-center mb-3">Login</h3>
            <hr>
            <div class="form-msg"></div>
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="record[user_login]" class="form-control rad" placeholder="Username" autocomplete="off" required>
            </div>

            <div class="form-group">
                <label>Password</label>
                <input type="password" name="record[user_password]" class="form-control rad" placeholder="Password" autocomplete="new-password" required>
            </div>

            <div class="third">
                <button type="submit" class="btn btn-primary btn-block third">Sign In</button>
            </div>


        </div>
        </form>
    </div>
</section>

{{ HTML::script('js/jquery.min.js') }}
{{ HTML::script('js/popper.min.js') }}
{{ HTML::script('js/bootstrap.min.js') }}
{{ HTML::script('js/sweetalert.min.js') }}
{{ HTML::script('js/validation.js') }}
{{ HTML::script('js/jquery-ui.js') }}
{{ HTML::script('admin/js/select2.min.js') }}
{{ HTML::script('admin/tinymce/js/tinymce/tinymce.min.js') }}
{{ HTML::script('admin/js/main.js') }}
</body>
</html>
