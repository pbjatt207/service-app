<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>{{ $page->page_title }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        {{ HTML::style('css/bootstrap.min.css') }}
    </head>
    <body>
        <div class="container">
            {!! $page->page_description !!}
        </div>
    </body>
</html>
